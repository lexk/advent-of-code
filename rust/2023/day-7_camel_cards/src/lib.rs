use std::cmp::Ordering;
use std::collections::{HashMap};
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;
use crate::HandRank::{FiveOfAKind, FourOfAKind, FullHouse, HighCard, OnePair, ThreeOfAKind, TwoPair};
use crate::Rank::{ACE, EIGHT, FIVE, FOUR, JACK, JOKER, KING, NINE, QUEEN, SEVEN, SIX, TEN, THREE, TWO};

pub enum GameMode { JACK, JOKER }


#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Copy, Clone)]
pub enum Rank {
    JOKER, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN,
    JACK, QUEEN, KING, ACE,
}

impl Rank {
    pub fn from_char(s: char, game_mode: &GameMode) -> Result<Rank, ()> {
        match s {
            '2' => Ok(TWO),
            '3' => Ok(THREE),
            '4' => Ok(FOUR),
            '5' => Ok(FIVE),
            '6' => Ok(SIX),
            '7' => Ok(SEVEN),
            '8' => Ok(EIGHT),
            '9' => Ok(NINE),
            'T' => Ok(TEN),
            'J' => Ok( match game_mode { GameMode::JACK => JACK, GameMode::JOKER => JOKER } ),
            'Q' => Ok(QUEEN),
            'K' => Ok(KING),
            'A' => Ok(ACE),
            _ => Err(())
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Copy, Clone)]
pub struct Card {
    rank: Rank,
}

impl Card {
    pub fn from_rank(char_card: Rank) -> Card {
        Card{rank: char_card }
    }
    pub fn from_char(tuple_card: char, game_mode: &GameMode) -> Card {
        Card {
            rank: Rank::from_char(tuple_card, &game_mode).unwrap(),
        }
    }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Clone)]
pub struct Hand {
    hand_srt: Vec<Card>,
    hand_raw: Vec<Card>,
    bet: usize
}

impl Hand {
    pub fn sort(mut self) -> Hand {
        self.hand_srt.sort();
        self
    }
    pub fn sum(&self) -> i32 {
        self.hand_srt.iter().map(|&card| card.rank.clone() as i32).sum()
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        self.hand_raw.cmp(&other.hand_raw) }
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub enum HandRank { HighCard, OnePair, TwoPair, ThreeOfAKind, FullHouse, FourOfAKind, FiveOfAKind, }

#[derive(PartialEq, Eq, Debug)]
pub struct RankedHand {
    hand_rank: HandRank,
    hand: Hand,
    bet: usize
}


impl RankedHand {
    pub fn new(hand: Hand) -> RankedHand {
        let mut rank_hm = HashMap::new();
        for card in hand.hand_srt.iter() {
            *rank_hm.entry(card.rank.clone()).or_insert(0) += 1;
        };
        match hand.hand_raw.iter().filter(|card| card.rank == JOKER).count() {
            0 => {},
            jokers @ _ => {
                for card in & mut rank_hm {
                    if card.0 != &JOKER {
                        *card.1 += jokers;}
                };
            }
        };
        let ranked = RankedHand::from_hand(&rank_hm).unwrap();
        RankedHand {
            hand_rank: ranked.clone(),
            hand: hand.clone(),
            bet: hand.bet
        }
    }

    fn from_hand(rank_hm: &HashMap<Rank, usize>) -> Result<HandRank, ()> {
        match RankedHand::check_n_of_a_kind(rank_hm) {
            found @ Ok(_) => return Ok(
                found.unwrap()),
            Err(_) => ()
        };
        match RankedHand::check_combination_high_card() {
            found_high@ Ok(_) => Ok(found_high.unwrap()),
            Err(_) => Err(())
        }
    }

    fn check_n_of_a_kind(rank_hm: &HashMap<Rank, usize>) -> Result<HandRank, ()> {
        match rank_hm.iter().map(|(_rank, &count)| count).max().unwrap() {
            5 => Ok(FiveOfAKind),
            4 => Ok(FourOfAKind),
            3 => match RankedHand::check_full_house(rank_hm) {
                Ok(_) => Ok(FullHouse),
                Err(_) => Ok(ThreeOfAKind)
            },
            2 => match RankedHand::check_for_pairs(rank_hm) {
                pair_found @ Ok(_) => pair_found,
                Err(_) => Err(())
            },
            _ => Err(())
        }
    }

    fn check_full_house(rank_hm: &HashMap<Rank, usize>) -> Result<HandRank, ()> {
        match &rank_hm.iter().map(|(_rank, &count)| count).collect::<Vec<usize>>()[..] {
            [2, 3] | [3, 2] => Ok(FullHouse),
            [3, 3, 1] | [1, 3, 3] | [3, 1, 3] if rank_hm.contains_key(&JOKER) => Ok(FullHouse),
            _ => Err(())
        }
    }

    fn check_for_pairs(rank_hm: &HashMap<Rank, usize>) -> Result<HandRank, ()> {
        match rank_hm.iter().filter(|(_rank, &val)| val == 2 ).count() {
            1 => Ok(OnePair),
            2 => Ok(TwoPair),
            3..=4 if rank_hm.contains_key(&JOKER) => Ok(OnePair),
            _ => Err(())
        }
    }

    fn check_combination_high_card() -> Result<HandRank, ()> {
        Ok(HighCard)
    }
}

impl PartialOrd for RankedHand{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for RankedHand {
    fn cmp(&self, other: &Self) -> Ordering {
        self.hand_rank.cmp(&other.hand_rank)
            .then(self.hand.cmp(&(other.hand)))
    }
}

pub fn ordered_ranked_hands<'a>(hands: &[&'a str], game_mode: &GameMode) -> Vec<(RankedHand, &'a str)> {
    let mut hands_vec: Vec<(RankedHand, &'a str)> = vec![];
    for hand in hands.iter() {
        let (hand_cards, hand_bet) = hand.split_once(' ').unwrap();
        let hnd: Vec<Card> = hand_cards.chars().map(|raw_card| Card::from_char(raw_card, &game_mode)).collect();
        hands_vec.push(
            (
                RankedHand::new(
                    Hand {
                        bet: hand_bet.parse().unwrap(),
                        hand_srt: hnd.clone(),
                        hand_raw: hnd
                    }.sort()
                ),
                hand
            )
        );
    };
    hands_vec.sort();
    hands_vec
}

pub fn camel_cards(file_path: &str, game_mode: &GameMode) -> Option<usize> {
    match read_lines(file_path) {
        Ok(file_exists) =>
            Some({
                let lines = file_exists.map(|line| line.unwrap()).collect::<Vec<String>>();
                ordered_ranked_hands(
                    &lines.iter().map(|line| line.as_str()).collect::<Vec<&str>>()[..],
                    game_mode
                )
                    .iter()
                    .enumerate()
                    .map(|r_hand|  r_hand.1.0.bet * (r_hand.0 + 1)).sum::<usize>()
            }),
        Err(_) => { println!("File not found, double-check the name!"); None }
    }
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}