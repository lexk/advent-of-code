use day_7_camel_cards::{camel_cards};
use day_7_camel_cards::GameMode::{JACK, JOKER};

#[test]
fn test_camel_cards_initial_test() {
    //given
    let file_path = "initial_test_case.txt";
    //when
    let calculated_total_winnings = camel_cards(file_path, &JACK);
    let expected_total_winnings = 6440;
    //then
    assert_eq!(calculated_total_winnings, Some(expected_total_winnings))
}

#[test]
fn test_camel_cards_input() {
    //given
    let file_path = "input.txt";

    //when
    let calculated_total_winnings = camel_cards(file_path, &JACK);
    let expected_total_winnings = 250_946_742;

    //then
    assert_eq!(calculated_total_winnings, Some(expected_total_winnings));
    println!(
        "The first half of Day-7 puzzle \nSum of total winnings: {:?}",
        calculated_total_winnings.unwrap()
    );
}
#[test]
fn test_camel_cards_jokers_initial_test() {
    //given
    let file_path = "initial_test_case.txt";
    //when
    let calculated_total_winnings = camel_cards(file_path, &JOKER);
    let expected_total_winnings = 5905;
    //then
    assert_eq!(calculated_total_winnings, Some(expected_total_winnings))
}

#[test]
fn test_camel_cards_jokers_input() {
    //given
    let file_path = "input.txt";

    //when
    let calculated_total_winnings = camel_cards(file_path, &JOKER);
    let expected_total_winnings = 251_824_095;

    //then
    assert_eq!(calculated_total_winnings, Some(expected_total_winnings));
    println!(
        "The second half of Day-7 puzzle \nSum of total winnings with Joker introduced: {:?}",
        calculated_total_winnings.unwrap()
    );
}