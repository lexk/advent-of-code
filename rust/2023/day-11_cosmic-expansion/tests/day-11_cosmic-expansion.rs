use day_11_cosmic_expansion::{RawUniverse, UniverseAge, UniverseBody, UniverseImage, UniversePairData};
use day_11_cosmic_expansion::UniverseBodyTypes::{Emptiness, Galaxy};

#[test]
fn test_raw_universe_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";

    //when
    let calculated_raw_universe = RawUniverse::universe_map_from_file(file_path);
    let expected_raw_universe = helper_get_initial_raw_universe();

    //then
    assert_eq!(calculated_raw_universe, Some(expected_raw_universe))
}


#[test]
fn test_young_universe_image_from_raw_universe_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";
    let raw_universe = RawUniverse::universe_map_from_file(file_path);

    //when
    let calculated_universe_image = UniverseImage::from_raw_universe(raw_universe.unwrap(), UniverseAge::Young);
    let expected_universe_image = helper_get_initial_universe_image();

    //then
    assert_eq!(calculated_universe_image, expected_universe_image)
}

#[test]
fn test_young_universe_pair_data_from_universe_image_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";
    let universe_image =
        UniverseImage::from_raw_universe(
            RawUniverse::universe_map_from_file(file_path).unwrap(),
            UniverseAge::Young
        );

    //when
    let calculated_universe_pair_data = UniversePairData::from_universe_image(&universe_image);
    let expected_universe_pair_data_len = (universe_image.data.len().pow(2) - universe_image.data.len())/2;

    //then
    assert_eq!(calculated_universe_pair_data.data.len(), expected_universe_pair_data_len)
}

#[test]
fn test_young_universe_pair_data_from_universe_image_input() {
    //given
    let file_path = "input.txt";
    let universe_image =
        UniverseImage::from_raw_universe(
            RawUniverse::universe_map_from_file(file_path).unwrap(),
            UniverseAge::Young
        );

    //when
    let calculated_universe_pair_data = UniversePairData::from_universe_image(&universe_image);
    let expected_universe_pair_data_len = (universe_image.data.len().pow(2) - universe_image.data.len())/2;

    //then
    assert_eq!(calculated_universe_pair_data.data.len(), expected_universe_pair_data_len)
}

#[test]
fn test_young_universe_pair_data_calculate_distances_initial() {
    //given
    let file_path = "initial_test_case.txt";
    let universe_pair_data =
        UniversePairData::from_universe_image(
            &UniverseImage::from_raw_universe(
                RawUniverse::universe_map_from_file(file_path).unwrap(),
                UniverseAge::Young
            )
        );

    //when
    let calculated_universe_pair_data_distances = universe_pair_data.calculate_distances();
    let expected_universe_pair_data_distances = 374;

    //then
    assert_eq!(calculated_universe_pair_data_distances, expected_universe_pair_data_distances)
}

#[test]
fn test_decade_universe_pair_data_calculate_distances_initial() {
    //given
    let file_path = "initial_test_case.txt";
    let universe_pair_data =
        UniversePairData::from_universe_image(
            &UniverseImage::from_raw_universe(
                RawUniverse::universe_map_from_file(file_path).unwrap(),
                UniverseAge::Decade
            )
        );

    //when
    let calculated_universe_pair_data_distances = universe_pair_data.calculate_distances();
    let expected_universe_pair_data_distances = 1_030;

    //then
    assert_eq!(calculated_universe_pair_data_distances, expected_universe_pair_data_distances)
}

#[test]
fn test_century_universe_pair_data_calculate_distances_initial() {
    //given
    let file_path = "initial_test_case.txt";
    let universe_pair_data =
        UniversePairData::from_universe_image(
            &UniverseImage::from_raw_universe(
                RawUniverse::universe_map_from_file(file_path).unwrap(),
                UniverseAge::Century
            )
        );

    //when
    let calculated_universe_pair_data_distances = universe_pair_data.calculate_distances();
    let expected_universe_pair_data_distances = 8_410;

    //then
    assert_eq!(calculated_universe_pair_data_distances, expected_universe_pair_data_distances)
}

#[test]
fn test_old_universe_pair_data_calculate_distances_initial() {
    //given
    let file_path = "initial_test_case.txt";
    let universe_pair_data =
        UniversePairData::from_universe_image(
            &UniverseImage::from_raw_universe(
                RawUniverse::universe_map_from_file(file_path).unwrap(),
                UniverseAge::Old
            )
        );

    //when
    let calculated_universe_pair_data_distances = universe_pair_data.calculate_distances();
    let expected_universe_pair_data_distances = 82_000_210;

    //then
    assert_eq!(calculated_universe_pair_data_distances, expected_universe_pair_data_distances)
}

#[test]
fn test_young_universe_pair_data_calculate_distances_input() {
    //given
    let file_path = "input.txt";
    let universe_pair_data =
        UniversePairData::from_universe_image(
            &UniverseImage::from_raw_universe(
                RawUniverse::universe_map_from_file(file_path).unwrap(),
                UniverseAge::Young
            )
        );

    //when
    let calculated_universe_pair_data_distances = universe_pair_data.calculate_distances();
    let expected_universe_pair_data_distances = 9_734_203;

    //then
    assert_eq!(calculated_universe_pair_data_distances, expected_universe_pair_data_distances)
}

#[test]
fn test_old_universe_pair_data_calculate_distances_input() {
    //given
    let file_path = "input.txt";
    let universe_pair_data =
        UniversePairData::from_universe_image(
            &UniverseImage::from_raw_universe(
                RawUniverse::universe_map_from_file(file_path).unwrap(),
                UniverseAge::Old
            )
        );

    //when
    let calculated_universe_pair_data_distances = universe_pair_data.calculate_distances();
    let expected_universe_pair_data_distances = 568_914_596_391;


    //then
    assert_eq!(calculated_universe_pair_data_distances, expected_universe_pair_data_distances)
}

fn helper_get_initial_raw_universe() -> RawUniverse {
    RawUniverse {
        data: vec![
            UniverseBody { body_type: Emptiness, x: 0, y: 0 },
            UniverseBody { body_type: Emptiness, x: 1, y: 0 },
            UniverseBody { body_type: Emptiness, x: 2, y: 0 },
            UniverseBody { body_type: Galaxy, x: 3, y: 0 },
            UniverseBody { body_type: Emptiness, x: 4, y: 0 },
            UniverseBody { body_type: Emptiness, x: 5, y: 0 },
            UniverseBody { body_type: Emptiness, x: 6, y: 0 },
            UniverseBody { body_type: Emptiness, x: 7, y: 0 },
            UniverseBody { body_type: Emptiness, x: 8, y: 0 },
            UniverseBody { body_type: Emptiness, x: 9, y: 0 },
            UniverseBody { body_type: Emptiness, x: 0, y: 1 },
            UniverseBody { body_type: Emptiness, x: 1, y: 1 },
            UniverseBody { body_type: Emptiness, x: 2, y: 1 },
            UniverseBody { body_type: Emptiness, x: 3, y: 1 },
            UniverseBody { body_type: Emptiness, x: 4, y: 1 },
            UniverseBody { body_type: Emptiness, x: 5, y: 1 },
            UniverseBody { body_type: Emptiness, x: 6, y: 1 },
            UniverseBody { body_type: Galaxy, x: 7, y: 1 },
            UniverseBody { body_type: Emptiness, x: 8, y: 1 },
            UniverseBody { body_type: Emptiness, x: 9, y: 1 },
            UniverseBody { body_type: Galaxy, x: 0, y: 2 },
            UniverseBody { body_type: Emptiness, x: 1, y: 2 },
            UniverseBody { body_type: Emptiness, x: 2, y: 2 },
            UniverseBody { body_type: Emptiness, x: 3, y: 2 },
            UniverseBody { body_type: Emptiness, x: 4, y: 2 },
            UniverseBody { body_type: Emptiness, x: 5, y: 2 },
            UniverseBody { body_type: Emptiness, x: 6, y: 2 },
            UniverseBody { body_type: Emptiness, x: 7, y: 2 },
            UniverseBody { body_type: Emptiness, x: 8, y: 2 },
            UniverseBody { body_type: Emptiness, x: 9, y: 2 },
            UniverseBody { body_type: Emptiness, x: 0, y: 3 },
            UniverseBody { body_type: Emptiness, x: 1, y: 3 },
            UniverseBody { body_type: Emptiness, x: 2, y: 3 },
            UniverseBody { body_type: Emptiness, x: 3, y: 3 },
            UniverseBody { body_type: Emptiness, x: 4, y: 3 },
            UniverseBody { body_type: Emptiness, x: 5, y: 3 },
            UniverseBody { body_type: Emptiness, x: 6, y: 3 },
            UniverseBody { body_type: Emptiness, x: 7, y: 3 },
            UniverseBody { body_type: Emptiness, x: 8, y: 3 },
            UniverseBody { body_type: Emptiness, x: 9, y: 3 },
            UniverseBody { body_type: Emptiness, x: 0, y: 4 },
            UniverseBody { body_type: Emptiness, x: 1, y: 4 },
            UniverseBody { body_type: Emptiness, x: 2, y: 4 },
            UniverseBody { body_type: Emptiness, x: 3, y: 4 },
            UniverseBody { body_type: Emptiness, x: 4, y: 4 },
            UniverseBody { body_type: Emptiness, x: 5, y: 4 },
            UniverseBody { body_type: Galaxy, x: 6, y: 4 },
            UniverseBody { body_type: Emptiness, x: 7, y: 4 },
            UniverseBody { body_type: Emptiness, x: 8, y: 4 },
            UniverseBody { body_type: Emptiness, x: 9, y: 4 },
            UniverseBody { body_type: Emptiness, x: 0, y: 5 },
            UniverseBody { body_type: Galaxy, x: 1, y: 5 },
            UniverseBody { body_type: Emptiness, x: 2, y: 5 },
            UniverseBody { body_type: Emptiness, x: 3, y: 5 },
            UniverseBody { body_type: Emptiness, x: 4, y: 5 },
            UniverseBody { body_type: Emptiness, x: 5, y: 5 },
            UniverseBody { body_type: Emptiness, x: 6, y: 5 },
            UniverseBody { body_type: Emptiness, x: 7, y: 5 },
            UniverseBody { body_type: Emptiness, x: 8, y: 5 },
            UniverseBody { body_type: Emptiness, x: 9, y: 5 },
            UniverseBody { body_type: Emptiness, x: 0, y: 6 },
            UniverseBody { body_type: Emptiness, x: 1, y: 6 },
            UniverseBody { body_type: Emptiness, x: 2, y: 6 },
            UniverseBody { body_type: Emptiness, x: 3, y: 6 },
            UniverseBody { body_type: Emptiness, x: 4, y: 6 },
            UniverseBody { body_type: Emptiness, x: 5, y: 6 },
            UniverseBody { body_type: Emptiness, x: 6, y: 6 },
            UniverseBody { body_type: Emptiness, x: 7, y: 6 },
            UniverseBody { body_type: Emptiness, x: 8, y: 6 },
            UniverseBody { body_type: Galaxy, x: 9, y: 6 },
            UniverseBody { body_type: Emptiness, x: 0, y: 7 },
            UniverseBody { body_type: Emptiness, x: 1, y: 7 },
            UniverseBody { body_type: Emptiness, x: 2, y: 7 },
            UniverseBody { body_type: Emptiness, x: 3, y: 7 },
            UniverseBody { body_type: Emptiness, x: 4, y: 7 },
            UniverseBody { body_type: Emptiness, x: 5, y: 7 },
            UniverseBody { body_type: Emptiness, x: 6, y: 7 },
            UniverseBody { body_type: Emptiness, x: 7, y: 7 },
            UniverseBody { body_type: Emptiness, x: 8, y: 7 },
            UniverseBody { body_type: Emptiness, x: 9, y: 7 },
            UniverseBody { body_type: Emptiness, x: 0, y: 8 },
            UniverseBody { body_type: Emptiness, x: 1, y: 8 },
            UniverseBody { body_type: Emptiness, x: 2, y: 8 },
            UniverseBody { body_type: Emptiness, x: 3, y: 8 },
            UniverseBody { body_type: Emptiness, x: 4, y: 8 },
            UniverseBody { body_type: Emptiness, x: 5, y: 8 },
            UniverseBody { body_type: Emptiness, x: 6, y: 8 },
            UniverseBody { body_type: Galaxy, x: 7, y: 8 },
            UniverseBody { body_type: Emptiness, x: 8, y: 8 },
            UniverseBody { body_type: Emptiness, x: 9, y: 8 },
            UniverseBody { body_type: Galaxy, x: 0, y: 9 },
            UniverseBody { body_type: Emptiness, x: 1, y: 9 },
            UniverseBody { body_type: Emptiness, x: 2, y: 9 },
            UniverseBody { body_type: Emptiness, x: 3, y: 9 },
            UniverseBody { body_type: Galaxy, x: 4, y: 9 },
            UniverseBody { body_type: Emptiness, x: 5, y: 9 },
            UniverseBody { body_type: Emptiness, x: 6, y: 9 },
            UniverseBody { body_type: Emptiness, x: 7, y: 9 },
            UniverseBody { body_type: Emptiness, x: 8, y: 9 },
            UniverseBody { body_type: Emptiness, x: 9, y: 9 }
        ]
    }
}

fn helper_get_initial_universe_image() -> UniverseImage {
    UniverseImage {
        data: vec![
            UniverseBody { body_type: Galaxy, x: 4, y: 0 },
            UniverseBody { body_type: Galaxy, x: 9, y: 1 },
            UniverseBody { body_type: Galaxy, x: 0, y: 2 },
            UniverseBody { body_type: Galaxy, x: 8, y: 5 },
            UniverseBody { body_type: Galaxy, x: 1, y: 6 },
            UniverseBody { body_type: Galaxy, x: 12, y: 7 },
            UniverseBody { body_type: Galaxy, x: 9, y: 10 },
            UniverseBody { body_type: Galaxy, x: 0, y: 11 },
            UniverseBody { body_type: Galaxy, x: 5, y: 11 }
        ]
    }
}

// #[test]
// fn test() {
//     //given
//
//     //when
//
//     //then
// }