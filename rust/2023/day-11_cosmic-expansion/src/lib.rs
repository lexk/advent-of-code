use std::cmp::Ordering;
use std::collections::HashSet;
use std::fs::File;
use std::hash::{Hash, Hasher};
use std::io;
use std::io::BufRead;
use std::path::Path;
use crate::Direction::{Column, Line};
use crate::UniverseAge::{Century, Decade, Old, Young};
use crate::UniverseBodyTypes::{Emptiness, Galaxy};

#[derive(PartialEq)]
pub enum Direction { Line, Column }

pub enum UniverseAge { Young=1, Decade=9, Century=99, Old=999_999 }

impl UniverseAge {
    fn as_usize(&self) -> usize {
        match self {
            UniverseAge::Young => {Young as usize}
            UniverseAge::Decade => {Decade as usize}
            UniverseAge::Century => {Century as usize}
            UniverseAge::Old => {Old as usize}
        }
    }
}

#[derive(Clone, Copy, Eq, PartialEq, PartialOrd, Debug, Hash)]
pub enum UniverseBodyTypes { Galaxy, Emptiness }

impl UniverseBodyTypes {
    pub fn from_char(ch: &char) -> Option<UniverseBodyTypes> {
        match ch {
            '#' => Some(Galaxy),
            '.' => Some(Emptiness),
            _ => None
        }
    }
}

#[derive(Clone, Copy, PartialEq, PartialOrd, Eq, Debug, Hash)]
pub struct UniverseBody {
    pub body_type: UniverseBodyTypes,
    pub x: usize,
    pub y: usize
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct RawUniverse {
    pub data: Vec<UniverseBody>
}

impl RawUniverse {
    pub fn universe_map_from_file(file_path: &str) -> Option<RawUniverse> {
        match read_lines(file_path) {
            Ok(file_exists) =>
                Some(
                    RawUniverse {
                        data: file_exists
                            .enumerate()
                            .filter_map(
                                |line|
                                    RawUniverse::universe_line_from_str(
                                        line.1.unwrap().as_ref(), line.0
                                    )
                                        .ok()
                            )
                            .fold(
                                Vec::<UniverseBody>::new(),
                                |mut vec_body, other_vec_body| {
                                    vec_body.extend(other_vec_body);
                                    vec_body
                                }
                            )
                    }
                ),
            Err(_) => { println!("File not found, double-check the name!"); None }
        }
    }

    fn universe_line_from_str(data: &str, y: usize) -> Result<Vec<UniverseBody>, ()> {
        match data.chars()
            .enumerate()
            .fold(
                Vec::<UniverseBody>::new(), |mut line, sym| {
                    match UniverseBodyTypes::from_char(&sym.1) {
                        None => None,
                        Some(dot_t) => Some(
                            line.push(
                                UniverseBody{ body_type: dot_t, y, x: sym.0}
                            )
                        )
                    };
                    line
                }
            ) {
            sym_vec if sym_vec.len() > 0 => Ok(sym_vec),
            _ => Err(())
        }
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct UniverseImage {
    pub data: Vec<UniverseBody>
}

impl UniverseImage {
    pub fn from_raw_universe(raw_universe: RawUniverse, universe_age: UniverseAge) -> UniverseImage {
        fn find_empty_by_direction(empty_vec: &mut Vec<UniverseBody>, raw_universe: &RawUniverse, direction: Direction) {
            for dir_idx in raw_universe.data
                .iter()
                .clone()
                .map(|raw_body| match direction {
                    Direction::Line => { raw_body.y}
                    Direction::Column => { raw_body.x}
                })
                .collect::<HashSet<usize>>() {
                match raw_universe.data
                    .iter()
                    .cloned()
                    .filter(
                        |body|
                            match direction {
                                Direction::Line => {body.y == dir_idx}
                                Direction::Column => {body.x == dir_idx}
                            }
                    ).collect::<Vec<UniverseBody>>() {
                    empty_lines
                    if empty_lines.iter().all(|body|  body.body_type == Emptiness )
                    => {empty_vec.extend(empty_lines)},
                    _ => {}
                }
            }
        }

        fn expand_universe_by_direction(
            expanded_data: &Vec<UniverseBody>,
            empty_lines_vec: &Vec<UniverseBody>,
            direction: Direction,
            universe_age: &UniverseAge
        ) -> Vec<UniverseBody> {
            expanded_data.iter().fold(
                Vec::<UniverseBody>::new(),
                |mut expanded_data_uni, un_body| {
                    let (bigger, _lesser): (Vec<UniverseBody>, _) = empty_lines_vec.iter()
                        .clone()
                        .partition(|&body| {
                            match direction {
                                Line => { un_body.x > body.x && un_body.y == body.y },
                                Column=> { un_body.y > body.y && un_body.x == body.x }
                            }
                        });
                    match bigger.len() {
                        0 => {expanded_data_uni.push(un_body.clone())},
                        n if un_body.body_type == Galaxy || direction == Line => {
                            expanded_data_uni.push(
                                UniverseBody {
                                    body_type: un_body.body_type.clone(),
                                    x: {
                                        match direction {
                                            Line =>  un_body.x + n * universe_age.as_usize(),
                                            Column=>  un_body.x
                                        }
                                    },
                                    y: {
                                        match direction {
                                            Line => {un_body.y},
                                            Column=> un_body.y + n * universe_age.as_usize()
                                        }
                                    },
                                }
                            )
                        },
                        _ => {expanded_data_uni.push(un_body.clone())},
                    }
                    expanded_data_uni
                }
            )
        }

        UniverseImage {
            data: {
                let (mut empty_lines_vec, mut empty_column_vec) = (Vec::<UniverseBody>::new(), Vec::<UniverseBody>::new());
                find_empty_by_direction(&mut empty_column_vec, &raw_universe, Column);
                let expanded_data = expand_universe_by_direction(&raw_universe.data.clone(), &empty_column_vec, Line, &universe_age);

                find_empty_by_direction(&mut empty_lines_vec, &RawUniverse{data: expanded_data.clone()}, Line);
                expand_universe_by_direction(&expanded_data, &empty_lines_vec, Column, &universe_age)
                    .iter()
                    .cloned()
                    .filter(|body| body.body_type == Galaxy ).collect()
            }
        }
    }
}

#[derive(Clone, Eq, PartialOrd, Debug)]
pub struct UniverseBodyPair {
    fst: UniverseBody,
    snd: UniverseBody
}

impl Hash for UniverseBodyPair {
    fn hash<H: Hasher>(&self, state: &mut H) {
        match self.fst.partial_cmp(&self.snd) {
            None => {}
            Some(Ordering::Greater) => {
                self.fst.hash(state);
                self.snd.hash(state)
            },
            Some(_) => {
                self.snd.hash(state);
                self.fst.hash(state)
            }
        }
    }
}

impl PartialEq for UniverseBodyPair {
    fn eq(&self, other: &Self) -> bool {
        ( self.fst == other.fst && self.snd == other.snd ) || (self.snd == other.fst && self.fst == other.snd )
    }
}

impl UniverseBodyPair {
    pub fn from_universe_bodies(fst: UniverseBody, snd: UniverseBody) -> UniverseBodyPair {
        UniverseBodyPair {fst, snd}
    }
}

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct UniversePairData {
    pub data: HashSet<UniverseBodyPair>
}

impl UniversePairData {
    pub fn from_universe_image(universe_image: &UniverseImage) -> UniversePairData {
        UniversePairData {
            data:
        universe_image.clone().data.iter()
            .fold(
                HashSet::<UniverseBodyPair>::new(),
                |mut pairs_hs, body_fst | {
                    for body_scd in universe_image.data.iter() {
                        if body_scd != body_fst {
                            pairs_hs
                                .insert(
                                    UniverseBodyPair::from_universe_bodies(
                                        body_fst.clone(),
                                        body_scd.clone()
                                    )
                                );
                        }
                    }
                    pairs_hs
                }
            )
        }

    }

    pub fn calculate_distances(&self) -> usize {
        self.data
            .iter()
            .fold(
                0usize,
                |sum, pair| {
                    sum + pair.fst.x.abs_diff(pair.snd.x)  + pair.fst.y.abs_diff(pair.snd.y)
                }
            )
    }
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}