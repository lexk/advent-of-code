use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io;
use std::io::{BufRead, BufReader, Lines};
use std::ops::Range;
use std::path::Path;
use crate::GardeningT::*;
use crate::SeedsInputMode::{RangeOfSeedsSingle, SingleSeed};

pub enum SeedsInputMode {SingleSeed, RangeOfSeedsSingle}

#[derive(Eq, PartialEq, Hash, Clone, Debug, PartialOrd, Ord)]
pub enum GardeningT { Seed, Soil, Fertilizer, Water, Light, Temperature, Humidity, Location, Default }

impl From<&&str> for GardeningT {
    fn from(value: &&str) -> Self {
        match value {
            &"seed" => Seed,
            &"soil" => Soil,
            &"fertilizer" => Fertilizer,
            &"water" => Water,
            &"light" => Light,
            &"temperature" => Temperature,
            &"humidity" => Humidity,
            &"location" => Location,
            &_ => Default
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct GardenAssetsRange { range: Vec<Range<usize>>, gardening_t: GardeningT }

impl Ord for GardenAssetsRange {
    fn cmp(&self, other: &Self) -> Ordering {
        self.range.clone().first().unwrap().start.cmp(&other.range.first().unwrap().start)
    }
}

impl PartialOrd for GardenAssetsRange {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.range.clone().first().unwrap().start.partial_cmp(&other.range.first().unwrap().start)
    }
}

impl GardenAssetsRange {
    pub fn transform(&self, assets_trans_map: &GardeningMapping, dest_gardening_t: &GardeningT) -> GardenAssetsRange {
        match assets_trans_map.asset_to_aset_map_ranges.get(&(self.gardening_t.clone(), dest_gardening_t.clone())) {
            None => {
                GardenAssetsRange{
                    range: vec![0usize..0usize],
                    gardening_t: Default
                }
            }
            Some(map) => {
                // GardenAssetsRange {
                //     range: map.iter()
                //         .fold(self.clone().range, |mut res, mp_range| {
                //             res = Vec::from_iter(
                //                 self.clone().range.iter().fold(HashSet::<Range<usize>>::new(), |mut hs_result, r_candidate| {
                //                     if hs_result.is_empty() {
                //                         hs_result = HashSet::from_iter(res.iter().cloned());
                //                     }
                //                     hs_result.remove(r_candidate);
                //                     for raw_r in range_construct(r_candidate, mp_range).iter().cloned() {
                //                         if raw_r.is_empty() {}
                //                         else { hs_result.insert(raw_r); };
                //                     }
                //                     hs_result
                //                 }).iter().cloned());
                //             res.sort_by_key(|k| k.start);
                //             res.iter().cloned().filter(|r| !r.is_empty()).collect()
                //             // res
                //         }),
                //     gardening_t: dest_gardening_t.clone()
                // }
                GardenAssetsRange {
                    range: map.iter()
                        .fold(self.clone().range, |mut res, mp_range| {
                            for r_candidate in self.clone().range.iter() {
                                res.extend(range_construct(r_candidate, mp_range).iter().cloned().filter(|ra| !ra.is_empty()).collect::<Vec<Range<usize>>>());
                                match res.iter().position(|p| p == r_candidate).unwrap_or(usize::MAX) {
                                    usize::MAX => {}
                                    found @ _ => {res.remove(found);}
                                }
                            }
                            res.sort_by_key(|k| k.start);
                            res.iter().cloned().filter(|r| !r.is_empty()).collect()
                        }),
                    gardening_t: dest_gardening_t.clone()
                }
            }
        }
    }

    pub fn from_raw(vec_ranges: Vec<Range<usize>>, gardening_t: GardeningT) -> GardenAssetsRange { GardenAssetsRange { range: vec_ranges, gardening_t} }

    pub fn get_vec_ranges(&self) -> Vec<Range<usize>> { self.range.clone() }

    pub fn get_gard_t(&self) -> GardeningT { self.gardening_t.clone()}
}

#[derive(Debug, Eq, PartialEq, Ord, Clone)]
pub struct GardenAsset { n: usize, gardening_t: GardeningT }

impl GardenAsset {
    pub fn transform(&self, assets_trans_map: &GardeningMapping, dest_gardening_t: GardeningT) -> GardenAsset {
        match
            match assets_trans_map.asset_to_aset_map_ranges.get(&(self.gardening_t.clone(), dest_gardening_t.clone())) {
                None => { 0 }
                Some(map) => {
                    map
                        .iter()
                        .fold(0usize, |res, mp_range| {
                            match (mp_range.src_start..(mp_range.src_start + mp_range.map_length)).contains(&self.n) {
                                true => { self.n + mp_range.dest_start - mp_range.src_start },
                                false => { res }
                            }
                        })
                }
            }
        {
            0 => { GardenAsset { n: self.n, gardening_t: dest_gardening_t.clone() } }
            found => { GardenAsset { n: found, gardening_t: dest_gardening_t.clone() } }
        }
    }

    pub fn from_raw(n: usize, gardening_t: GardeningT) -> Self { Self {n, gardening_t} }

    pub fn get_n(&self) -> usize { self.n }
}

impl PartialOrd for GardenAsset {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.n.partial_cmp(&other.n)
    }
}

#[derive(Eq, Hash, PartialEq, Clone, Debug, Ord)]
pub struct GardeningMapRange {
    src_start: usize,
    dest_start: usize,
    map_length: usize
}

impl GardeningMapRange {
    pub fn from_raw(from_start: usize, to_start: usize, map_length: usize) -> Self {
        GardeningMapRange { src_start: from_start, dest_start: to_start, map_length}
    }
    pub fn get_s_end(&self) -> usize {
        self.src_start + self.map_length -1
    }
    pub fn get_d_end(&self) -> usize {
        self.dest_start + self.map_length -1
    }
}

impl PartialOrd for GardeningMapRange {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.dest_start.partial_cmp(&other.dest_start)
    }
}

#[derive(Eq, PartialEq, Debug)]
pub struct GardeningMapping {
    asset_to_aset_map_ranges: HashMap<(GardeningT, GardeningT), Vec<GardeningMapRange>>
}

impl GardeningMapping {
    pub fn new() -> Self {
        Self { asset_to_aset_map_ranges: HashMap::<(GardeningT, GardeningT), Vec<GardeningMapRange>>::new() }
    }
    pub fn from_map(map: HashMap<(GardeningT, GardeningT), Vec<GardeningMapRange>>) -> Self {
        Self { asset_to_aset_map_ranges: map }
    }
}

pub fn gardening_iterative_seeds(file_path: &str, seeds_input_mode: SeedsInputMode) -> Option<(Vec<GardenAsset>, GardeningMapping)> {
    match read_lines(file_path) {
        Ok(file_exists) =>
            Some({
                let ((raw_seeds, mapping), _asset_t_pair) = parse_lines_to_vec_assets_mapping(file_exists);
                match seeds_input_mode {
                    SingleSeed => {
                        (
                            raw_seeds.iter()
                                .map( |seed_n|
                                    GardenAsset {
                                        n: seed_n.clone(),
                                        gardening_t: Seed
                                    }
                                )
                                .collect::<Vec<GardenAsset>>(),
                            mapping
                        )

                    }
                    RangeOfSeedsSingle => {
                        (
                            raw_seeds.chunks(2)
                                .fold(Vec::<GardenAsset>::new(), |mut assets, seed_rng| {
                                    assets.extend((seed_rng[0]..seed_rng[0]+seed_rng[1]).into_iter()
                                        .map(|single_seed_n|
                                            GardenAsset {
                                                n: single_seed_n.clone(),
                                                gardening_t: Seed
                                            }
                                        )
                                        .collect::<Vec<GardenAsset>>());
                                    assets
                                }),
                            mapping
                        )
                    }
                }
            }
            ),
        Err(_) => { println!("File not found, double-check the name!"); None}
    }
}
pub fn gardening_range_seeds(file_path: &str) -> Option<(Vec<GardenAssetsRange>, GardeningMapping)> {
    match read_lines(file_path) {
        Ok(file_exists) =>
            Some({
                let ((raw_seeds, mapping), _asset_t_pair) = parse_lines_to_vec_assets_mapping(file_exists);
                (
                    raw_seeds.chunks(2)
                        .fold(Vec::<GardenAssetsRange>::new(), |mut ranges, seed_rng| {
                            ranges.push(GardenAssetsRange {gardening_t: Seed, range: vec![seed_rng[0]..seed_rng[0]+seed_rng[1]]});
                            ranges
                        }),
                    mapping
                )
            }
            ),
        Err(_) => { println!("File not found, double-check the name!"); None}
    }
}

pub fn range_construct(r_candidate: &Range<usize>, mp_range: &GardeningMapRange) -> Vec<Range<usize>> {
    match r_candidate {
        cand_sub_set
        if (cand_sub_set.start >= mp_range.src_start) &&
            (cand_sub_set.end <= mp_range.get_s_end() ) => {
            vec![
                0..0,
                mp_range.dest_start + (cand_sub_set.start - mp_range.src_start)..(mp_range.get_d_end() - (mp_range.get_s_end() - cand_sub_set.end)),
                0..0
            ]
        }
        cand_left_inters
        if (cand_left_inters.start >= mp_range.src_start) &&
            (mp_range.get_s_end() <= cand_left_inters.end) &&
            (cand_left_inters.end.wrapping_sub(mp_range.get_s_end()) < cand_left_inters.len() ) => {
            vec![
                0..0,
                mp_range.dest_start + (cand_left_inters.start - mp_range.src_start)..mp_range.get_d_end()+1,
                mp_range.get_s_end()+1..cand_left_inters.end
            ]
        }
        cand_right_inters
        if (cand_right_inters.start <= mp_range.src_start) &&
            (mp_range.get_s_end() >= cand_right_inters.end) &&
            (mp_range.src_start.wrapping_sub(cand_right_inters.start) < cand_right_inters.len()) => {
            vec![
                cand_right_inters.start..mp_range.src_start,
                mp_range.dest_start..(mp_range.get_d_end() - (mp_range.get_s_end() - cand_right_inters.end )),
                0..0
            ]
        }
        cand_super_set
        if (cand_super_set.start <= mp_range.get_s_end()) &&
            (mp_range.src_start <= cand_super_set.end) => {
            vec![
                cand_super_set.start..mp_range.src_start,
                mp_range.dest_start..mp_range.get_d_end().clone()+1,
                mp_range.get_s_end()+1..cand_super_set.end
            ]
        }
        _ => {
            vec![
                0..0,
                r_candidate.clone(),
                0..0
            ]
        }
    }
}

fn parse_lines_to_vec_assets_mapping(file_exists: Lines<BufReader<File>>) -> ((Vec<usize>, GardeningMapping), (GardeningT, GardeningT)) {
    file_exists
        .fold(
            ((Vec::<usize>::new(), GardeningMapping::new()), (Default, Default)),
            |((raw_seeds, mut mapping), asset_t), line| {
                match line {
                    Ok(delim) if delim.is_empty() => {
                        ((raw_seeds, mapping), (Default, Default))
                    },
                    Ok(seeds) if seeds.contains("seeds:") => {
                        (
                            (seeds
                                 .split_once(": ")
                                 .unwrap_or(("", "")).1
                                 .trim()
                                 .split_terminator(" ")
                                 .map(|num| num.parse().unwrap())
                                 .collect::<Vec<usize>>(),
                             mapping),
                            asset_t
                        )

                    },
                    Ok(map) if map.contains("-to-") => {
                        let map_namings_v = map.split_terminator("-to-").collect::<Vec<&str>>();
                        (
                            (
                                raw_seeds,
                                mapping
                            ),
                            (
                                GardeningT::from(map_namings_v.first().unwrap()),
                                GardeningT::from(&map_namings_v.get(1).unwrap().split_whitespace().next().unwrap()),
                            )
                        )
                    },
                    Ok(range) => {
                        let range_nums = range
                            .split_terminator(" ")
                            .map(|c| c.parse().unwrap())
                            .collect::<Vec<usize>>();
                        (
                            (raw_seeds,
                             {
                                 match mapping.asset_to_aset_map_ranges.entry(asset_t.clone()).or_insert(vec![]) {
                                     entry_v @ &mut _ => {
                                         entry_v.push(
                                             GardeningMapRange {
                                                 src_start: range_nums.get(1).unwrap().clone(),
                                                 dest_start:  range_nums.get(0).unwrap().clone(),
                                                 map_length:  range_nums.get(2).unwrap().clone()
                                             }
                                         );
                                         entry_v.sort()
                                     }
                                 }
                                 mapping
                             }),
                            asset_t
                        )
                    },
                    Err(_) => {
                        ((raw_seeds, mapping), asset_t)
                    }
                }
            }
        )
}

pub fn transform_vec_garden_assets(vec_assets: Vec<GardenAsset>, mapping: &GardeningMapping, next_state: &GardeningT) -> Vec<GardenAsset> {
    vec_assets.iter()
        .map(|asset| asset.transform(mapping, next_state.clone()))
        .collect::<Vec<GardenAsset>>()
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}