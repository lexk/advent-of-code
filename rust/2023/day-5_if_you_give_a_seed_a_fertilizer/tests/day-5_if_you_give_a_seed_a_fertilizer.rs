use std::collections::HashMap;
use std::ops::Range;
use day_5_if_you_give_a_seed_a_fertilizer::{GardenAsset, GardenAssetsRange, gardening_iterative_seeds, gardening_range_seeds, GardeningMapping, GardeningMapRange, range_construct, transform_vec_garden_assets};
use day_5_if_you_give_a_seed_a_fertilizer::GardeningT::{Seed, Soil, Fertilizer, Water, Light, Temperature, Humidity, Location, Default};
use day_5_if_you_give_a_seed_a_fertilizer::SeedsInputMode::{RangeOfSeedsSingle, SingleSeed};


#[test]
fn test_gardening_single_assets_initial_test_case() {
    //given
    let file_path: &str = "initial_test_case.txt";

    //when
    let expected_gardening_vec_soil: (Vec<GardenAsset>, GardeningMapping) = get_gardening_assets_single_initial_test_case();
    let calculated_gardening_vec_soil = gardening_iterative_seeds(file_path, SingleSeed).unwrap();

    assert_eq!(calculated_gardening_vec_soil, expected_gardening_vec_soil)
}

#[test]
fn test_gardening_range_assets_initial_test_case() {
    //given
    let file_path: &str = "initial_test_case.txt";

    //when
    let expected_gardening_vec_soil = get_gardening_assets_ranges_initial_test_case();
    let calculated_gardening_vec_soil = gardening_range_seeds(file_path).unwrap();

    assert_eq!(calculated_gardening_vec_soil, expected_gardening_vec_soil)
}

#[test]
fn test_single_garden_asset_transform_initial_test_case() {
    //given
    let file_path: &str = "initial_test_case.txt";
    let (seeds, mapping) = gardening_iterative_seeds(file_path, SingleSeed).unwrap();

    //when
    let expected_soil = vec![
        GardenAsset::from_raw(81, Soil),
        GardenAsset::from_raw(14, Soil),
        GardenAsset::from_raw(57, Soil),
        GardenAsset::from_raw(13, Soil),
    ];
    let calculated_soil = transform_vec_garden_assets(seeds, &mapping, &Soil);

    assert_eq!(calculated_soil, expected_soil);

    //when
    let expected_fertilizer = vec![
        GardenAsset::from_raw(81, Fertilizer),
        GardenAsset::from_raw(53, Fertilizer),
        GardenAsset::from_raw(57, Fertilizer),
        GardenAsset::from_raw(52, Fertilizer),

    ];
    let calculated_fertilizer = transform_vec_garden_assets(calculated_soil, &mapping, &Fertilizer);

    assert_eq!(calculated_fertilizer, expected_fertilizer);

    //when
    let expected_water = vec![
        GardenAsset::from_raw(81, Water),
        GardenAsset::from_raw(49, Water),
        GardenAsset::from_raw(53, Water),
        GardenAsset::from_raw(41, Water),

    ];
    let calculated_water = transform_vec_garden_assets(calculated_fertilizer, &mapping, &Water);

    assert_eq!(calculated_water, expected_water);
    
    //when
    let expected_light = vec![
        GardenAsset::from_raw(74, Light),
        GardenAsset::from_raw(42, Light),
        GardenAsset::from_raw(46, Light),
        GardenAsset::from_raw(34, Light),

    ];
    let calculated_light = transform_vec_garden_assets(calculated_water, &mapping, &Light);

    assert_eq!(calculated_light, expected_light);
    
    //when
    let expected_temperature = vec![
        GardenAsset::from_raw(78, Temperature),
        GardenAsset::from_raw(42, Temperature),
        GardenAsset::from_raw(82, Temperature),
        GardenAsset::from_raw(34, Temperature),

    ];
    let calculated_temperature = transform_vec_garden_assets(calculated_light, &mapping, &Temperature);

    assert_eq!(calculated_temperature, expected_temperature);
    
    //when
    let expected_humidity = vec![
        GardenAsset::from_raw(78, Humidity),
        GardenAsset::from_raw(43, Humidity),
        GardenAsset::from_raw(82, Humidity),
        GardenAsset::from_raw(35, Humidity),

    ];
    let calculated_humidity = transform_vec_garden_assets(calculated_temperature, &mapping, &Humidity);

    assert_eq!(calculated_humidity, expected_humidity);
    
    //when
    let expected_location = vec![
        GardenAsset::from_raw(82, Location),
        GardenAsset::from_raw(43, Location),
        GardenAsset::from_raw(86, Location),
        GardenAsset::from_raw(35, Location),

    ];
    let calculated_location = transform_vec_garden_assets(calculated_humidity, &mapping, &Location);

    assert_eq!(calculated_location, expected_location)
}

#[test]
fn test_single_garden_asset_transform_input() {
    //given
    let file_path: &str = "input.txt";
    let garden_conversion_t_seq = [Soil, Fertilizer, Water, Light, Temperature, Humidity, Location];
    let (seeds, mapping) = gardening_iterative_seeds(file_path, SingleSeed).unwrap();

    //when
    let mut calculated_assets_after_seq_transformation = garden_conversion_t_seq.iter()
        .fold(
            Vec::<GardenAsset>::new(),
            |converted_assets, source_t | {
                if converted_assets.is_empty() {
                    transform_vec_garden_assets(seeds.clone(), &mapping, source_t)
                } else {
                    transform_vec_garden_assets(converted_assets, &mapping, source_t)
                }
            }
        );
    calculated_assets_after_seq_transformation.sort();
    let calculated_lowest_n_location = calculated_assets_after_seq_transformation.first().unwrap();
    let expected_lowest_n_location = &GardenAsset::from_raw(278_755_257, Location);

    //then
    assert_eq!(calculated_lowest_n_location, expected_lowest_n_location);

    println!(
        "The first half of Day-5 puzzle \nThe Lowest location number is: {:?}",
        calculated_lowest_n_location.get_n()
    );
}

#[test]
fn test_range_construct_initial_test_case_first_range_all_permutations(){
    //given
    let candidate = 79..93;
    let map_ranges = HashMap::from([
        ("cand_sub_set", GardeningMapRange::from_raw(70, 0, 30)),
        ("cand_left_inters", GardeningMapRange::from_raw(70, 15, 15)),
        ("cand_right_inters", GardeningMapRange::from_raw(85, 10, 50)),
        ("cand_super_set", GardeningMapRange::from_raw(82, 10, 10)),
        ("cand_not_overlay", GardeningMapRange::from_raw(0, 10, 50))
    ]);

    //when
    let calculated_hm_new_ranges = map_ranges.iter()
        .fold(
            HashMap::<String, Vec<Range<usize>>>::new(),
            |mut hm_results, map_range| {
                 hm_results.entry(map_range.0.to_string()).or_insert(range_construct(&candidate, map_range.1));
                hm_results
            }
        );
    let elements_sum_calculated_new_ranges = calculated_hm_new_ranges.iter()
        .fold(
            HashMap::<String, usize>::new(),
            |mut sum_hm, rm| {
                sum_hm.entry(rm.0.clone()).or_insert(rm.1.iter().map(|vr| vr.len() ).sum::<usize>());
                sum_hm
            }
        );
    let expected_hm_new_ranges = HashMap::from(
        [
            ("cand_sub_set".to_string(), vec![0..0, 9..23, 0..0]),
            ("cand_left_inters".to_string(), vec![0..0, 24..30, 85..93]),
            ("cand_right_inters".to_string(), vec![79..85, 10..18, 0..0]),
            ("cand_super_set".to_string(), vec![79usize..82, 10..20, 92..93]),
            ("cand_not_overlay".to_string(), vec![0..0, 79..93, 0..0]),
        ]
    );


    assert!(elements_sum_calculated_new_ranges.iter().all(|range| range.1 == &candidate.len()));
    assert_eq!(calculated_hm_new_ranges, expected_hm_new_ranges);
}

#[test]
fn test_single_range_iterative_garden_asset_transform_initial_test_case() {
    //given
    let file_path: &str = "initial_test_case.txt";
    let garden_conversion_t_seq = [Soil, Fertilizer, Water, Light, Temperature, Humidity, Location];
    let (seeds, mapping) = gardening_iterative_seeds(file_path, RangeOfSeedsSingle).unwrap();

    //when
    let mut calculated_assets_after_seq_transformation = garden_conversion_t_seq.iter()
        .fold(
            Vec::<GardenAsset>::new(),
            |converted_assets, source_t | {
                if converted_assets.is_empty() {
                    transform_vec_garden_assets(seeds.clone(), &mapping, source_t)
                } else {
                    transform_vec_garden_assets(converted_assets, &mapping, source_t)
                }
            }
        );
    calculated_assets_after_seq_transformation.sort();
    let calculated_lowest_n_location = calculated_assets_after_seq_transformation.first().unwrap();
    let expected_lowest_n_location = &GardenAsset::from_raw(46, Location);

    //then
    assert_eq!(calculated_lowest_n_location, expected_lowest_n_location);
}

#[test]
fn test_range_garden_asset_transform_initial_test_case() {
    //given
    let file_path: &str = "initial_test_case.txt";
    let garden_conversion_t_seq = [Soil, Fertilizer, Water, Light, Temperature, Humidity, Location];
    let (seeds, mapping) = gardening_range_seeds(file_path).unwrap();

    //when
    let mut calculated_assets_after_seq_transformation = garden_conversion_t_seq.iter()
        .fold(
            Vec::<GardenAssetsRange>::new(),
            |converted_assets, source_t | {
                if converted_assets.is_empty() { seeds.clone() } else { converted_assets.clone() }
                    .iter()
                    .map(|r| r.transform(&mapping, source_t))
                    .collect()
            }
        );
    calculated_assets_after_seq_transformation.sort();
    let calculated_lowest_n_location = calculated_assets_after_seq_transformation.first().unwrap().get_vec_ranges().first().unwrap().clone();
    let expected_lowest_n_location = 46usize..56;

    //then
    assert_eq!(calculated_lowest_n_location, expected_lowest_n_location);
}

#[test]
fn test_range_garden_asset_transform_input() {
    //given
    let file_path: &str = "input.txt";
    let garden_conversion_t_seq = [Soil, Fertilizer, Water, Light, Temperature, Humidity, Location];
    let (seeds, mapping) = gardening_range_seeds(file_path).unwrap();

    //when
    let mut calculated_assets_after_seq_transformation = garden_conversion_t_seq.iter()
        .fold(
            Vec::<GardenAssetsRange>::new(),
            |converted_assets, source_t | {
                if converted_assets.is_empty() {
                    seeds.clone().iter()
                        .map(|r| r.transform(&mapping, source_t))
                        .filter(|gr| gr.get_gard_t() != Default )
                        .collect()
                } else {
                    converted_assets.clone().iter()
                        .map(|r| r.transform(&mapping, source_t))
                        .filter(|gr| gr.get_gard_t() != Default )
                        .collect()
                }
            }
        );
    calculated_assets_after_seq_transformation.sort();
    let calculated_lowest_n_location = calculated_assets_after_seq_transformation.first().unwrap().get_vec_ranges().first().unwrap().clone();
    let expected_lowest_n_location = 0..26829166;

    //then
    assert_eq!(calculated_lowest_n_location, expected_lowest_n_location);
}

#[test]
#[ignore]
fn test_range_iterative_garden_asset_transform_input() {
    //given
    let file_path: &str = "input.txt";
    let garden_conversion_t_seq = [Soil, Fertilizer, Water, Light, Temperature, Humidity, Location];
    let (seeds, mapping) = gardening_iterative_seeds(file_path, RangeOfSeedsSingle).unwrap();

    //when
    let mut calculated_assets_after_seq_transformation = garden_conversion_t_seq.iter()
        .fold(
            Vec::<GardenAsset>::new(),
            |converted_assets, source_t | {
                if converted_assets.is_empty() {
                    transform_vec_garden_assets(seeds.clone(), &mapping, source_t)
                } else {
                    transform_vec_garden_assets(converted_assets, &mapping, source_t)
                }
            }
        );
    calculated_assets_after_seq_transformation.sort();
    let calculated_lowest_n_location = calculated_assets_after_seq_transformation.first().unwrap();
    let expected_lowest_n_location = &GardenAsset::from_raw(278_755_257, Location);

    //then
    assert_eq!(calculated_lowest_n_location, expected_lowest_n_location);

    println!(
        "The Second half of Day-5 puzzle \nThe Lowest location number is: {:?}",
        calculated_lowest_n_location.get_n()
    );
}

fn get_gardening_assets_single_initial_test_case() -> (Vec<GardenAsset>, GardeningMapping) {
    (
        vec![
            GardenAsset::from_raw( 79,  Seed ),
            GardenAsset::from_raw( 14,  Seed ),
            GardenAsset::from_raw( 55,  Seed ),
            GardenAsset::from_raw( 13,  Seed )
        ],
        GardeningMapping::from_map(
                HashMap::from(
                    [
                        (
                            (Seed, Soil),
                            vec![
                                GardeningMapRange::from_raw( 98, 50, 2 ),
                                GardeningMapRange::from_raw( 50, 52, 48 )
                            ]
                        ),
                        (
                            (Soil, Fertilizer),
                            vec![
                                GardeningMapRange::from_raw( 15, 0, 37 ),
                                GardeningMapRange::from_raw( 52, 37, 2 ),
                                GardeningMapRange::from_raw( 0, 39, 15 )
                            ]
                        ),
                        (
                            (Fertilizer, Water),
                            vec![
                                GardeningMapRange::from_raw( 11, 0, 42 ),
                                GardeningMapRange::from_raw( 0, 42, 7 ),
                                GardeningMapRange::from_raw( 53, 49, 8 ),
                                GardeningMapRange::from_raw( 7, 57, 4 )]),
                        (
                            (Water, Light),
                            vec![
                                GardeningMapRange::from_raw( 25, 18, 70 ),
                                GardeningMapRange::from_raw( 18, 88, 7 )
                            ]
                        ),
                        (
                            (Light, Temperature),
                            vec![
                                GardeningMapRange::from_raw( 77, 45, 23 ),
                                GardeningMapRange::from_raw( 64, 68, 13 ),
                                GardeningMapRange::from_raw( 45, 81, 19 )
                            ]
                        ),
                        (
                            (Temperature, Humidity),
                            vec![
                                GardeningMapRange::from_raw( 69, 0, 1 ),
                                GardeningMapRange::from_raw( 0, 1, 69 )
                            ]
                        ),
                        (
                            (Humidity, Location),
                            vec![
                                GardeningMapRange::from_raw( 93, 56, 4 ),
                                GardeningMapRange::from_raw( 56, 60, 37 )
                            ]
                        ),
                    ]
                )
        )
        
    )
}

fn get_gardening_assets_ranges_initial_test_case() -> (Vec<GardenAssetsRange>, GardeningMapping ) {
    (
        vec![
            GardenAssetsRange::from_raw(vec![79..93], Seed ),
            GardenAssetsRange::from_raw(vec![55..68], Seed )
        ],
        GardeningMapping::from_map(
            HashMap::from(
                [
                    (
                        (Seed, Soil),
                        vec![
                            GardeningMapRange::from_raw( 98, 50, 2 ),
                            GardeningMapRange::from_raw( 50, 52, 48 )
                        ]
                    ),
                    (
                        (Soil, Fertilizer),
                        vec![
                            GardeningMapRange::from_raw( 15, 0, 37 ),
                            GardeningMapRange::from_raw( 52, 37, 2 ),
                            GardeningMapRange::from_raw( 0, 39, 15 )
                        ]
                    ),
                    (
                        (Fertilizer, Water),
                        vec![
                            GardeningMapRange::from_raw( 11, 0, 42 ),
                            GardeningMapRange::from_raw( 0, 42, 7 ),
                            GardeningMapRange::from_raw( 53, 49, 8 ),
                            GardeningMapRange::from_raw( 7, 57, 4 )]),
                    (
                        (Water, Light),
                        vec![
                            GardeningMapRange::from_raw( 25, 18, 70 ),
                            GardeningMapRange::from_raw( 18, 88, 7 )
                        ]
                    ),
                    (
                        (Light, Temperature),
                        vec![
                            GardeningMapRange::from_raw( 77, 45, 23 ),
                            GardeningMapRange::from_raw( 64, 68, 13 ),
                            GardeningMapRange::from_raw( 45, 81, 19 )
                        ]
                    ),
                    (
                        (Temperature, Humidity),
                        vec![
                            GardeningMapRange::from_raw( 69, 0, 1 ),
                            GardeningMapRange::from_raw( 0, 1, 69 )
                        ]
                    ),
                    (
                        (Humidity, Location),
                        vec![
                            GardeningMapRange::from_raw( 93, 56, 4 ),
                            GardeningMapRange::from_raw( 56, 60, 37 )
                        ]
                    ),
                ]
            )
        )
    )


}