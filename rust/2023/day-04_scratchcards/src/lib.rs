use std::collections::HashMap;
use std::fs::File;
use std::{io, str};
use std::cmp::{Ordering, PartialOrd};
use std::io::BufRead;
use std::path::Path;

#[derive(Eq, Hash, Ord, Debug, Copy, Clone)]
pub struct Card {
    number: u32,
    winning_n_count: u8
}

impl Card {
    pub fn new(number: u32, winning_n_count: u8) -> Card { Card { number, winning_n_count } }
    pub fn from_string(line_raw: String) -> Card {
        let (card_and_win_num, elf_num) = line_raw.split_once('|').unwrap();
        let elf_num_vec: Vec<&str> = elf_num.split_terminator(&[' '][..])
            .filter(|empty| empty != &"")
            .collect();
        let (car_n, win_num) = card_and_win_num.split_once(':').unwrap();
        Card {
            number: car_n.split_once(' ').unwrap().1.trim().parse().unwrap(),
            winning_n_count: {
                win_num
                    .split_terminator(&[' '][..])
                    .map(|w_num| elf_num_vec.iter().any(|m| m == &w_num))
                    .filter(|c_res| c_res != &false)
                    .count() as u8
            }
        }
    }
}

impl PartialEq for Card {
    fn eq(&self, other: &Self) -> bool {
        self.number == other.number
    }
}

#[derive(Eq, Debug, Clone)]
pub struct Pile {
    pub cards: HashMap<Card, usize>
}

impl PartialEq for Pile {
    fn eq(&self, other: &Self) -> bool {
        self.cards == other.cards
    }
}

impl PartialOrd for Card {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.number.cmp(&other.number))
    }
}

impl Pile {

    pub fn new() -> Pile {
        Pile { cards: HashMap::<Card, usize>::new() }
    }
    pub fn from_file(file_path: &str) -> Pile {
        Pile {
            cards: {
                match read_lines(file_path) {
                    Err(_) => { println!("File not found, double-check the name!"); HashMap::<Card, usize>::new() },
                    file_exists @ Ok(_) => {
                        file_exists.unwrap()
                            .fold(HashMap::<Card, usize>::new(), |mut pile, line_res| {
                                let line_raw = line_res.unwrap();
                                *pile.entry(Card::from_string(line_raw)).or_insert(0) +=1;
                                pile
                            })
                    }
                }
            }
        }
    }

    pub fn winning_cards(&self, order: &(&Card, &usize)) -> Pile {
        match order.0.winning_n_count {
            0 => {Pile::new()},
            _ => {
                Pile {
                    cards:
                    self.cards
                        .iter()
                        .filter_map(|c| {
                            if c.0 > order.0 && (c.0.number - order.0.number) as u8 <= order.0.winning_n_count {
                                Ok((c.0.clone(), order.1.clone()))
                            }
                            else { Err(()) }
                        }.ok())
                        .collect::<HashMap<Card, usize>>()
                }

            }
        }
    }

    pub fn winning_cards_pile(&self) -> Pile {
        match self.cards.is_empty() {
            true => {Pile::new()}
            false => {
                let mut sorted_cards = self.cards.keys().collect::<Vec<&Card>>();
                sorted_cards.sort();
                sorted_cards.iter()
                    .fold(Pile::new(), |res, card| {
                        if res.cards.is_empty() {
                            self.add_piles(self.winning_cards(&self.cards.get_key_value(card).unwrap()))
                        } else {
                            res.add_piles(res.winning_cards(&res.cards.get_key_value(card).unwrap()))
                        }
                    })

            }
        }
    }

    fn add_piles(&self, other: Pile) -> Pile {
        Pile {
            cards: self.cards.iter()
                .fold(
                    HashMap::<Card, usize>::new(),
                    |mut ext, single| {
                        *ext.entry(single.0.clone()).or_insert(0) +=
                            match other.cards.get(single.0) {
                                None => { single.1.clone() }
                                Some(card_count) => { single.1 + card_count }
                            };
                        ext
                    }
                )
        }
    }
}

pub fn scratchcards_win_points(file_path: &str) -> usize {
    match read_lines(file_path) {
        file_exists @ Ok(_) => {
            file_exists.unwrap()
                .fold(0usize, |points, line_res| {
                    let line_raw = line_res.unwrap();
                    match Card::from_string(line_raw).winning_n_count {
                        0 => points,
                        1 => points + 1,
                        new_p @ _ => { points + 2_u32.pow((new_p - 1) as u32) as usize },
                    }
                })
        },
        Err(_) => { println!("File not found, double-check the name!"); 999 }
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}