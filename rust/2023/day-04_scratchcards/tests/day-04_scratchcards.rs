use day_04_scratchcards::{scratchcards_win_points, Card, Pile};
use std::collections::HashMap;

#[test]
fn test_scratchcards_win_points_with_initial_data() {
    //given
    let initial_set_path = "./initial_test_case.txt";

    //when
    let expected_points = 13usize;
    let calculated_points = scratchcards_win_points(initial_set_path);

    //then
    assert_eq!(expected_points, calculated_points)
}

#[test]
fn test_scratchcards_win_points_with_actual_data() {
    //given
    let initial_set_path = "./input.txt";

    //when
    let expected_points = 21138usize;
    let calculated_points = scratchcards_win_points(initial_set_path);

    //then
    assert_eq!(expected_points, calculated_points);

    println!(
        "The first half of Day-2 puzzle\n
            Points score from winning scratchcards_win_points is {}",
        calculated_points
    )
}

#[test]
fn test_scratchcards_win_cards_with_initial_data() {
    //given
    let initial_set_path = "./initial_test_case.txt";

    //when
    let expected_pile = get_pile_initial_test_case();
    let calculated_pile = Pile::from_file(initial_set_path);

    //then
    assert_eq!(expected_pile, calculated_pile);
}

#[test]
fn test_pile_winning_cards_with_initial_data() {
    //given
    let initial_set_path = "./initial_test_case.txt";
    let initial_pile = Pile::from_file(initial_set_path);

    //when
    let expected_winning_mul = Pile {
        cards: HashMap::from([
            (Card::new(2, 2), 3),
            (Card::new(3, 2), 3),
            (Card::new(4, 1), 3),
            (Card::new(5, 0), 3),
        ]),
    };
    let calculated_winning_mul = initial_pile.winning_cards(&(&Card::new(1, 4), &3));

    //then
    assert_eq!(expected_winning_mul, calculated_winning_mul);
}

#[test]
fn test_pile_winning_cards_pile_with_initial_data() {
    //given
    let initial_set_path = "./initial_test_case.txt";
    let key_pile = Pile::from_file(initial_set_path);

    //when
    let expected_winning_pile = Pile {
        cards: {
            HashMap::from([
                (Card::new(1, 4), 1),
                (Card::new(2, 2), 2),
                (Card::new(3, 2), 4),
                (Card::new(4, 1), 8),
                (Card::new(5, 0), 14),
                (Card::new(6, 0), 1),
            ])
        },
    };
    let expected_winning_pile_card_sum = 30;

    let calculated_winning_pile = key_pile.winning_cards_pile();
    let calculated_winning_pile_card_sum = calculated_winning_pile.cards.values().sum::<usize>();

    //then
    assert_eq!(
        calculated_winning_pile_card_sum,
        expected_winning_pile_card_sum
    );
    assert_eq!(calculated_winning_pile, expected_winning_pile);
}

#[test]
fn test_pile_winning_cards_pile_with_actual_data() {
    //given
    let initial_set_path = "./input.txt";
    let key_pile = Pile::from_file(initial_set_path);

    //when
    let expected_winning_pile_card_sum = 7_185_540;

    let calculated_winning_pile = key_pile.winning_cards_pile();
    let calculated_winning_pile_card_sum = calculated_winning_pile.cards.values().sum::<usize>();

    //then
    assert_eq!(
        calculated_winning_pile_card_sum,
        expected_winning_pile_card_sum
    );
    println!(
        "The second half of Day-2 puzzle\n
            Cards pile sum from winning_cards_pile is: {}",
        calculated_winning_pile_card_sum
    )
}

fn get_pile_initial_test_case() -> Pile {
    Pile {
        cards: {
            HashMap::from([
                (Card::new(1, 4), 1),
                (Card::new(2, 2), 1),
                (Card::new(3, 2), 1),
                (Card::new(4, 1), 1),
                (Card::new(5, 0), 1),
                (Card::new(6, 0), 1),
            ])
        },
    }
}
