use day_02_cube_conundrum::Color::{Blue, Green, Red};
use day_02_cube_conundrum::{
    guess_cubes_game, minimal_cubes_set_for_game, sum_power_min_cubes, Cubes, Game, Round,
};

#[test]
fn test_cubes_from_str() {
    //given
    let cubes_raw = "16 blue";
    let bag_limit = Round {
        cubes: vec![
            Cubes {
                color: Red,
                count: 999,
            },
            Cubes {
                color: Green,
                count: 999,
            },
            Cubes {
                color: Blue,
                count: 999,
            },
        ],
    };

    //when
    let expected_cubes = Cubes {
        color: Blue,
        count: 16,
    };
    let calculated_cubes = Cubes::from_str(cubes_raw, &bag_limit).unwrap();

    //then
    assert_eq!(expected_cubes, calculated_cubes)
}

#[test]
fn test_round_from_str() {
    //given
    let round_raw = "7 green, 5 blue, 4 red";
    let bag_limit = Round {
        cubes: vec![
            Cubes {
                color: Red,
                count: 999,
            },
            Cubes {
                color: Green,
                count: 999,
            },
            Cubes {
                color: Blue,
                count: 999,
            },
        ],
    };

    //when
    let expected_round = Round {
        cubes: vec![
            Cubes {
                color: Green,
                count: 7,
            },
            Cubes {
                color: Blue,
                count: 5,
            },
            Cubes {
                color: Red,
                count: 4,
            },
        ],
    };
    let calculated_round = Round::from_str(round_raw, &bag_limit).unwrap();

    //then
    assert_eq!(expected_round, calculated_round)
}

#[test]
fn test_game_from_str() {
    // given
    let line_game =
        "Game 3: 5 red, 9 blue, 1 green; 5 red; 11 red, 2 green, 8 blue; 2 green, 6 blue";
    let bag_limit = Round {
        cubes: vec![
            Cubes {
                color: Red,
                count: 999,
            },
            Cubes {
                color: Green,
                count: 999,
            },
            Cubes {
                color: Blue,
                count: 999,
            },
        ],
    };

    // when
    let expected_game = Game {
        num: 3,
        rounds: vec![
            Round {
                cubes: vec![
                    Cubes {
                        color: Red,
                        count: 5,
                    },
                    Cubes {
                        color: Blue,
                        count: 9,
                    },
                    Cubes {
                        color: Green,
                        count: 1,
                    },
                ],
            },
            Round {
                cubes: vec![Cubes {
                    color: Red,
                    count: 5,
                }],
            },
            Round {
                cubes: vec![
                    Cubes {
                        color: Red,
                        count: 11,
                    },
                    Cubes {
                        color: Green,
                        count: 2,
                    },
                    Cubes {
                        color: Blue,
                        count: 8,
                    },
                ],
            },
            Round {
                cubes: vec![
                    Cubes {
                        color: Green,
                        count: 2,
                    },
                    Cubes {
                        color: Blue,
                        count: 6,
                    },
                ],
            },
        ],
    };

    let calculated_game = Game::from_str(line_game, &bag_limit).unwrap();

    // then
    assert_eq!(expected_game, calculated_game)
}

#[test]
fn test_guess_cubes_game() {
    //given
    let bag_contain_path = "./bag_contain.txt";
    let input_path = "./input.txt";

    //when
    let expected_possible_games_sum = 2162usize;
    let calculated_possible_games_sum = guess_cubes_game(bag_contain_path, input_path);

    //then
    assert_eq!(expected_possible_games_sum, calculated_possible_games_sum)
}

#[test]
fn test_pick_bigger_cubes() {
    //given
    let cube = Cubes {
        color: Red,
        count: 3,
    };
    let candidate_round = Round {
        cubes: vec![
            Cubes {
                color: Red,
                count: 5,
            },
            Cubes {
                color: Blue,
                count: 9,
            },
            Cubes {
                color: Green,
                count: 1,
            },
        ],
    };

    //when
    let expected_cube = Cubes {
        color: Red,
        count: 5,
    };
    let calculated_cube = cube.pick_bigger_cubes(&candidate_round);

    //then
    assert_eq!(expected_cube, calculated_cube)
}

#[test]
fn test_minimal_cubes_set_for_game() {
    //given
    let game = Game {
        num: 3,
        rounds: vec![
            Round {
                cubes: vec![
                    Cubes {
                        color: Red,
                        count: 5,
                    },
                    Cubes {
                        color: Blue,
                        count: 9,
                    },
                    Cubes {
                        color: Green,
                        count: 1,
                    },
                ],
            },
            Round {
                cubes: vec![Cubes {
                    color: Red,
                    count: 5,
                }],
            },
            Round {
                cubes: vec![
                    Cubes {
                        color: Red,
                        count: 11,
                    },
                    Cubes {
                        color: Green,
                        count: 2,
                    },
                    Cubes {
                        color: Blue,
                        count: 8,
                    },
                ],
            },
            Round {
                cubes: vec![
                    Cubes {
                        color: Green,
                        count: 2,
                    },
                    Cubes {
                        color: Blue,
                        count: 6,
                    },
                ],
            },
        ],
    };

    //when
    let expected_minimal_set = Round {
        cubes: vec![
            Cubes {
                color: Red,
                count: 11,
            },
            Cubes {
                color: Green,
                count: 2,
            },
            Cubes {
                color: Blue,
                count: 9,
            },
        ],
    };
    let calculated_minimal_set = minimal_cubes_set_for_game(&game);

    //then
    assert_eq!(expected_minimal_set, calculated_minimal_set);
}

#[test]
fn test_sum_power_min_cubes() {
    //given
    let input_path = "./input.txt";

    //when
    let expected_possible_games_sum = 72513usize;
    let calculated_possible_games_sum = sum_power_min_cubes(input_path);
    //then
    assert_eq!(expected_possible_games_sum, calculated_possible_games_sum)
}
