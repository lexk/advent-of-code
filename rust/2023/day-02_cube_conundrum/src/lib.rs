use crate::Color::{Blue, Green, Red};
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub enum Color {
    Blue,
    Green,
    Red,
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct Cubes {
    pub color: Color,
    pub count: usize,
}

impl Cubes {
    pub fn from_str(score: &str, limit: &Round) -> Result<Cubes, ()> {
        match ["red", "green", "blue"]
            .iter()
            .map(|color| score.find(color))
            .collect::<Vec<Option<usize>>>()[..]
        {
            [Some(_), None, None] => Cubes {
                color: Red,
                count: score.split_once(' ').unwrap().0.parse::<usize>().unwrap(),
            }
            .validate_cubes_max(limit),
            [None, Some(_), None] => Cubes {
                color: Green,
                count: score.split_once(' ').unwrap().0.parse::<usize>().unwrap(),
            }
            .validate_cubes_max(limit),
            [None, None, Some(_)] => Cubes {
                color: Blue,
                count: score.split_once(' ').unwrap().0.parse::<usize>().unwrap(),
            }
            .validate_cubes_max(limit),
            _ => Err(()),
        }
    }
    pub fn validate_cubes_max(self, bag_contain: &Round) -> Result<Self, ()> {
        match self.count
            <= bag_contain
                .cubes
                .iter()
                .find(|cube| cube.color == self.color)
                .unwrap()
                .count
        {
            true => Ok(self),
            false => Err(()),
        }
    }

    pub fn pick_bigger_cubes(mut self, candidate: &Round) -> Self {
        match candidate.cubes.iter().find(|cube| cube.color == self.color) {
            None => self,
            Some(bigger) => {
                if self.count >= bigger.count {
                    self
                } else {
                    self.count = bigger.count;
                    self
                }
            }
        }
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct Round {
    pub cubes: Vec<Cubes>,
}

impl Round {
    pub fn from_str(round_raw: &str, limit: &Round) -> Result<Round, ()> {
        match round_raw
            .split(", ")
            .map(|revel| Cubes::from_str(revel, limit))
            .collect::<Vec<Result<Cubes, ()>>>()
        {
            found @ Vec { .. } if !found.contains(&Err(())) => Ok(Round {
                cubes: found
                    .iter()
                    .map(|cube| cube.clone().unwrap())
                    .collect::<Vec<Cubes>>(),
            }),
            _ => Err(()),
        }
    }

    pub fn limit_from_str(round_raw: &str) -> Result<Round, ()> {
        Round::from_str(
            round_raw,
            &Round {
                cubes: vec![
                    Cubes {
                        color: Red,
                        count: 999,
                    },
                    Cubes {
                        color: Green,
                        count: 999,
                    },
                    Cubes {
                        color: Blue,
                        count: 999,
                    },
                ],
            },
        )
    }
}

#[derive(Debug, PartialEq)]
pub struct Game {
    pub num: usize,
    pub rounds: Vec<Round>,
}

impl Game {
    pub fn from_str(game_line: &str, bag_limit: &Round) -> Result<Game, ()> {
        match game_line.split_once(": ").unwrap() {
            (game_num, game_score) => {
                match game_score
                    .split("; ")
                    .map(|turn| Round::from_str(turn, bag_limit))
                    .collect::<Vec<Result<Round, ()>>>()
                {
                    rounds_err @ Vec { .. } if rounds_err.contains(&Err(())) => Err(()),
                    rounds_ok => Ok(Game {
                        rounds: rounds_ok
                            .iter()
                            .map(|round| round.clone().unwrap())
                            .collect(),
                        num: game_num
                            .split_once(" ")
                            .unwrap()
                            .1
                            .parse::<usize>()
                            .unwrap(),
                    }),
                }
            }
        }
    }
}

pub fn line_to_vec_game(file_path: &str, bag_limit: &Round) -> Option<Vec<Game>> {
    match read_lines(file_path) {
        Ok(file_exist) => Some(
            file_exist
                .filter_map(|line| Game::from_str(line.unwrap().as_ref(), &bag_limit).ok())
                .collect::<Vec<Game>>(),
        ),
        Err(_) => {
            println!("File not found, double-check the name!");
            None
        }
    }
}

pub fn guess_cubes_game(bag_contain_path: &str, input_path: &str) -> usize {
    let bag_limit = match read_lines(bag_contain_path) {
        Ok(limit_file) => limit_file
            .map(|line| Round::limit_from_str(line.unwrap().as_ref()).unwrap())
            .collect::<Vec<Round>>()[0]
            .clone(),
        Err(_) => Round {
            cubes: vec![
                Cubes {
                    color: Red,
                    count: 999,
                },
                Cubes {
                    color: Green,
                    count: 999,
                },
                Cubes {
                    color: Blue,
                    count: 999,
                },
            ],
        },
    };

    println!(
        "The first half of Day-2 puzzle \nBag limit: {:?}",
        bag_limit
    );

    let initial_lines = line_to_vec_game(input_path, &bag_limit).unwrap();
    println!("Possible games count: {}", initial_lines.len());

    let sum_games_num = initial_lines.iter().map(|game| game.num).sum::<usize>();
    println!("Sum of Possible games numbers: {}", sum_games_num);

    sum_games_num
}

pub fn minimal_cubes_set_for_game(game: &Game) -> Round {
    game.rounds.iter().fold(
        Round {
            cubes: vec![
                Cubes {
                    color: Red,
                    count: 0,
                },
                Cubes {
                    color: Green,
                    count: 0,
                },
                Cubes {
                    color: Blue,
                    count: 0,
                },
            ],
        },
        |mut max_round, current_r| {
            max_round = Round {
                cubes: max_round
                    .cubes
                    .iter()
                    .map(|cube| cube.clone().pick_bigger_cubes(current_r))
                    .collect(),
            };
            max_round
        },
    )
}

pub fn sum_power_min_cubes(input_path: &str) -> usize {
    let bag_limit = Round {
        cubes: vec![
            Cubes {
                color: Red,
                count: 999,
            },
            Cubes {
                color: Green,
                count: 999,
            },
            Cubes {
                color: Blue,
                count: 999,
            },
        ],
    };

    println!("The Second half of Day-2 puzzle");

    let initial_lines = line_to_vec_game(input_path, &bag_limit).unwrap();
    println!("Possible games count: {}", initial_lines.len());

    let min_set = initial_lines
        .iter()
        .map(|game| minimal_cubes_set_for_game(game))
        .collect::<Vec<Round>>();

    let sum_of_power = min_set
        .iter()
        .map(|round| {
            round
                .cubes
                .iter()
                .map(|cube| cube.count)
                .fold(0usize, |mut res, f_usize| {
                    if res == 0 {
                        f_usize
                    } else {
                        res *= f_usize;
                        res
                    }
                })
        })
        .sum::<usize>();

    println!("Sum of power of minimal sets: {}", sum_of_power);

    sum_of_power
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
