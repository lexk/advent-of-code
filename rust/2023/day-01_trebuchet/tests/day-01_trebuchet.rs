use day_01_trebuchet::{trebuchet_alphanumeric_calibration, trebuchet_numeric_calibration};

#[test]
fn test_trebuchet_first_part_numeric_final_input() {
    //given
    let test_case_path = "./input.txt";

    //when
    let expected_count = 54605;
    let calculated_count = trebuchet_numeric_calibration(test_case_path).unwrap();

    //then
    assert_eq!(expected_count, calculated_count);

    println!(
        "The first half of Day-1 puzzle \
        Sum of calibration numeric values from input: {}",
        calculated_count
    )
}

#[test]
fn test_trebuchet_first_part_numeric_initial_test_case_num() {
    //given
    let test_case_path = "./initial_test_case_num.txt";

    //when
    let expected_count = 142;
    let calculated_count = trebuchet_numeric_calibration(test_case_path).unwrap();

    //then
    assert_eq!(expected_count, calculated_count);

    println!(
        "The first half of Day-1 puzzle \
        Sum of calibration values from initial test case: {}",
        calculated_count
    )
}
#[test]
fn test_trebuchet_second_part_alphanumeric_initial_test_case_alpha_num() {
    //given
    let test_case_path = "./initial_test_case_alphanum.txt";

    //when
    let expected_count = 281;
    let calculated_count = trebuchet_alphanumeric_calibration(test_case_path).unwrap();

    //then
    assert_eq!(expected_count, calculated_count);

    println!(
        "The second half of Day-1 puzzle \
        Sum of calibration values from initial test case with written numbers: {}",
        calculated_count
    )
}

#[test]
fn test_trebuchet_second_part_alphanumeric_final_input() {
    //given
    let test_case_path = "./input.txt";

    //when
    let expected_count = 55_429;
    let calculated_count = trebuchet_alphanumeric_calibration(test_case_path).unwrap();

    //then
    assert_eq!(expected_count, calculated_count);

    println!(
        "The second half of Day-1 puzzle \
        Sum of calibration alphanumeric values from input with written numbers: {}",
        calculated_count
    )
}
