use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;
use text2num::{text2digits, Language};

pub const NUM: [&str; 9] = [
    "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
];

pub fn trebuchet_alphanumeric_calibration(file_path: &str) -> Option<i32> {
    match read_lines(file_path) {
        Ok(file_exist) => {
            let initial_lines = file_exist
                .map(|line| line.unwrap())
                .collect::<Vec<String>>();
            let initial_numbers = initial_lines
                .iter()
                .map(|line| {
                    line.chars()
                        .enumerate()
                        .filter(|order_c| order_c.1.is_ascii_digit())
                        .collect::<Vec<(usize, char)>>()
                })
                .collect::<Vec<Vec<(usize, char)>>>();
            let spelled_nums_idx = initial_lines
                .iter()
                .map(|line| {
                    NUM.map(|num| {
                        line.match_indices(num)
                            .into_iter()
                            .collect::<Vec<(usize, &str)>>()
                    })
                    .iter()
                    .filter(|l| l.len() != 0)
                    .fold(
                        Vec::<(usize, String)>::new(),
                        |mut res: Vec<(usize, String)>, num_occ| {
                            for (idx, num) in num_occ {
                                res.push((*idx, num.to_string()))
                            }
                            res
                        },
                    )
                })
                .collect::<Vec<Vec<(usize, String)>>>();

            let line_convert_nums_idx = spelled_nums_idx
                .iter()
                .map(|line| {
                    line.iter()
                        .filter_map(|word| match text2digits(&word.1, &Language::english()) {
                            Ok(found) => Some(
                                (word.0, found.chars().next().unwrap()), // format!("{}", found).parse::<u8>().unwrap() as char)
                            ),
                            Err(_) => None,
                        })
                        // .filter(|res| res.is_some())
                        // .map(|wr_res| wr_res.unwrap())
                        .collect::<Vec<(usize, char)>>()
                })
                .collect::<Vec<Vec<(usize, char)>>>();
            let result = initial_numbers
                .iter()
                .zip(line_convert_nums_idx.iter())
                .map(|line_pair| {
                    let mut s_vec =
                        [line_pair.0.clone(), line_pair.1.clone()].concat::<(usize, char)>();
                    s_vec.sort_by_key(|key| key.0);
                    s_vec
                        .iter()
                        .cloned()
                        .map(|c_pair| c_pair.1)
                        .collect::<String>()
                })
                .collect::<Vec<String>>()
                .iter()
                .map(
                    |line_form| {
                        [
                            line_form.chars().next().unwrap(),
                            line_form.chars().last().unwrap(),
                            10u8 as char,
                        ]
                        .iter()
                        .collect::<String>()
                    }, // .parse::<u32>().unwrap()
                )
                .collect::<Vec<String>>();
            Some(
                result
                    .iter()
                    .cloned()
                    .map(|st| {
                        st.split_once(|x| x == (10u8 as char))
                            .unwrap()
                            .0
                            .parse::<i32>()
                            .unwrap()
                    })
                    .sum::<i32>(),
            )
        }
        Err(_) => {
            println!("File not found, double-check the name!");
            None
        }
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub fn trebuchet_numeric_calibration(file_path: &str) -> Option<i32> {
    match read_lines(file_path) {
        file_exists @ Ok(_) => Some(
            file_exists
                .unwrap()
                .map(|line| {
                    line.unwrap()
                        .chars()
                        .filter(|c: &char| c.is_ascii_digit())
                        .collect::<String>()
                })
                .collect::<Vec<String>>()
                .iter()
                .map(|line_n| match line_n.len() {
                    0 => 0,
                    1 => (line_n.clone() + line_n).parse().unwrap(),
                    _ => [
                        line_n.chars().next().unwrap(),
                        line_n.chars().last().unwrap(),
                    ]
                    .iter()
                    .collect::<String>()
                    .parse()
                    .unwrap(),
                })
                .sum::<i32>(),
        ),
        Err(_) => {
            println!("File not found, double-check the name!");
            None
        }
    }
}
