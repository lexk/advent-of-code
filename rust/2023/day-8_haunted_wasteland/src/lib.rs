use std::cmp::Ordering;
use std::collections::{BTreeSet};
use std::fs::File;
use std::{io};
use std::io::BufRead;
use std::path::Path;
use num::Integer;
use crate::Instruction::{Left, Right};
use crate::Point::*;

const POINTS: [Point; 26] =  [A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z];

#[derive(Debug, Copy, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub enum Point {
    A=0, B, C, D, E, F, G, H, I, J, K, L,
    M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z
}

impl Point {
    pub fn as_u8(&self) -> u8 {
        self.clone() as isize as u8
    }
}

#[derive(Debug, Copy, Clone, Ord, PartialOrd, Eq)]
pub struct Node {
    pub data: [Point; 3]
}

impl Node {
    pub fn from_str(raw_p: &str) -> Node {
        Node {
            data:
            <[Point; 3]>::try_from(raw_p.chars()
                .map(|raw_c|
                    POINTS
                        .iter()
                        .find(|po| po.as_u8() == raw_c as u8 - 'A' as u8)
                        .copied()
                        .unwrap()
                )
                .collect::<Vec<Point>>()).unwrap()
        }
    }
}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.data.last().unwrap().eq(&other.data.last().unwrap())
    }
}

#[derive(Debug, Copy, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub enum Instruction {Left, Right}

impl Instruction {
    pub fn from_char(instr: &char) -> Result<Instruction, ()> {
        match instr { &'L' => Ok(Left), &'R' => Ok(Right), _ => Err(()) }
    }
}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub struct Instructions {
    pub directions: Vec<Instruction>
}

impl Instructions {
    pub fn from_str(dir: &str) -> Result<Instructions, ()> {
        Ok(Instructions {
            directions: dir.chars()
                    .filter_map(|c| Instruction::from_char(&c).ok()).collect::<Vec<Instruction>>()
        })
    }

    pub fn from_file(file_path: &str) -> Instructions {
        Instructions::from_str(
            read_lines(file_path)
                .unwrap()
                .next()
                .unwrap()
                .unwrap()
                .as_ref()
        ).unwrap()
    }

}

#[derive(Debug, Copy, Clone, PartialOrd, PartialEq, Eq)]
pub struct Branch {
    pub value: Node,
    pub left: Node,
    pub right: Node,
}

impl Ord for Branch {
    fn cmp(&self, other: &Self) -> Ordering {
        self.value.data.cmp(&other.value.data)
    }
}

impl Branch {
    pub fn new(value: Node, left: Node, right: Node) -> Self {
        Branch {
            value,
            left,
            right
        }
    }

    pub fn from_str(line: &str) -> Result<Branch, ()> {
        match line
            .split_terminator(&['=', ' ', '(', ',', ')'][..])
            .filter(|w| w != &"")
            .collect::<Vec<&str>>()[..] {
            [value, left, right] => Ok(
                Branch {
                    value: Node::from_str(value),
                    left: Node::from_str(left),
                    right: Node::from_str(right)
                }
            ),
            _ => Err(())
        }
    }

    pub fn get_leaf(&self, direction: &Instruction) -> Node {
        match direction {
            Left => {self.left.clone()}
            Right => {self.right.clone()}
        }
    }

}

#[derive(Debug, Clone, PartialOrd, Ord, PartialEq, Eq)]
pub struct Tree {
    pub branches: BTreeSet<Branch>
}

impl Tree {
    pub fn new() -> Tree { Tree { branches: BTreeSet::<Branch>::new() } }

    pub fn from_vec_string(v_strings: &Vec<String>) -> Tree {
        v_strings
            .iter()
            .fold(Tree::new(), |mut br, line| {
                br.branches.insert(Branch::from_str(line.as_ref()).unwrap());
                br
            })
    }

    pub fn from_file(file_path: &str) -> Tree {
        Tree::from_vec_string(
            &read_lines(file_path)
                .unwrap()
                .filter_map(|l| l.ok())
                .collect::<Vec<String>>()[2..]
                .to_vec()
        )
    }

    pub fn hop_to_next_node(&self, current_point: Option<Node>, instruction: &Instruction) -> Option<Node> {
        Some(
            self.branches
                .get(
                    &Branch {
                        value: current_point.unwrap_or_else(|| Node {data: [A, A, A]} ),
                        left: Node {data: [W, W, W]},
                        right: Node {data: [W, W, W]}
                    }
                )
                .unwrap()
                .get_leaf(&instruction)
        )
    }

    pub fn next_hop_multiple_nodes(&self, current_points: &mut Vec<Node>, instruction: &Instruction) {
            current_points.iter_mut().for_each(|n| *n = self.branches.get(
                &Branch {
                    value: *n,
                    left: Node {data: [W, W, W]},
                    right: Node {data: [W, W, W]}
                }
            )
                .unwrap()
                .get_leaf(&instruction)
            )
    }

    pub fn sync_stepping_to_dest_node(
        &self,
        current_points: &mut Vec<(usize, Node)>,
        dest_node: &Node,
        initial_directions: &Instructions

    ) -> (usize, Node) {
        let mut max_current_point = current_points.iter().cloned().max().unwrap();
        while !current_points.iter().all(|c| (c == &max_current_point) && (c.0 > 0)) {
            for current_point in current_points.iter_mut() {
                'single_end: while
                ( &max_current_point.1 != dest_node )
                    ||
                    (
                        (current_point.0 < max_current_point.0)
                            || (current_point != &max_current_point)
                    )
                    || current_point.0 == 0
                {
                    initial_directions.directions.iter().for_each(|dir|
                        {
                            *current_point = (current_point.0 + 1, self.hop_to_next_node(Some(current_point.1.clone()), &dir).unwrap());
                        }

                    );
                    if &current_point.1 == dest_node { break 'single_end }
                };
            }
            max_current_point = current_points.iter().cloned().max().unwrap();
        }
        max_current_point
    }

    pub fn predict_steps_count_to_destinations(
        &self,
        current_points: &mut Vec<(usize, Node)>,
        dest_node: &Node,
        initial_directions: &Instructions
    ) -> usize {
        for current_point in current_points.iter_mut() {
            while &current_point.1 != dest_node {
                initial_directions.directions.iter().for_each(|dir| {
                    *current_point = (
                        current_point.0 + 1,
                        self.hop_to_next_node(Some(current_point.1), &dir).unwrap()
                    );
                }
                );
            };
        }
       current_points.iter().fold(1, |acc, steps| acc.lcm(&steps.0))
    }

    pub fn find_staring_points(&self) -> Vec<Node> {
        self.branches
            .iter()
            .filter(|b| b.value == Node {data: [A, A, A]})
            // .filter(|b| b.value.data.last().unwrap() == &A)
            .map(|b| b.value)
            .collect()
    }
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
