use std::collections::BTreeSet;
use day_8_haunted_wasteland::{Branch, Instructions, Node, read_lines, Tree};
use day_8_haunted_wasteland::Instruction::{Left, Right};
use day_8_haunted_wasteland::Point::*;


#[test]
fn test_first_part_branch_from_str() {
    //given
    let node_raw = "AAA = (BBB, CCC)";

    //when
    let calculated_branch = Branch::from_str(node_raw);
    let expected_branch = Branch {
        value: Node { data: [A,A,A]},
        left: Node { data: [B,B,B]},
        right: Node { data: [C,C,C]}
    };

    //then
    assert_eq!(calculated_branch.unwrap(), expected_branch)
}

#[test]
fn test_first_part_instructions_from_str() {
    //given
    let raw_instr = "LLR";

    //when
    let calculated_instructions = Instructions::from_str(raw_instr);
    let expected_instructions = Instructions{ directions: vec![Left, Left, Right] };

    //then
    assert_eq!(calculated_instructions.unwrap(), expected_instructions)
}

#[test]
fn test_first_part_instructions_initial_test_case(){
    //given
    let file_path = "initial_test_case.txt";

    //when
    let calculated_directions = Instructions::from_file(file_path);
    let expected_directions = Instructions{ directions: vec![Right, Left] };

    //then
    assert_eq!(calculated_directions, expected_directions)
}

#[test]
fn test_first_part_points_single_case() {
    //given
    let point = "AAA";

    //when
    let calculated_point = Node::from_str(point);
    let expected_point = Node { data: [A, A, A]};

    //then
    assert_eq!(calculated_point, expected_point)

}

#[test]
fn test_first_part_branch_initial_test_case(){
    //given
    let file_path = "initial_test_case.txt";
    let buf = read_lines(file_path);

    //when
    let tree = buf.unwrap().filter_map(|l| l.ok()).collect::<Vec<String>>();
    let cut_branch = &tree[2..];
    let calculated_branch = Branch::from_str(cut_branch.first().cloned().unwrap().as_ref());
    let expected_branch = Branch { value: Node { data: [A,A,A]}, left: Node { data: [B,B,B]}, right: Node { data: [C,C,C]}};

    //then
    assert_eq!(calculated_branch, Ok(expected_branch))
}

#[test]
fn test_first_part_branch_get_leaf() {
    //given
    let initial_tree = helper_get_initial_test_case_tree();
    let instructions = Instructions{ directions: vec![Left, Left, Right] };

    //when
    let calculated_leaf = initial_tree.branches.first().unwrap().get_leaf(instructions.directions.first().unwrap());
    let expected_leaf = Node { data: [B,B,B]};

    //then
    assert_eq!(calculated_leaf, expected_leaf)
}

#[test]
fn test_first_part_tree_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";

    //when
    let calculated_tree = Tree::from_file(file_path);
    let expected_tree = helper_get_initial_test_case_tree();

    //then
    assert_eq!(calculated_tree, expected_tree)
}

#[test]
fn test_first_part_tree_next_hop_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = helper_get_initial_test_case_tree();

    //when
    //first
    let calculated_second_point = initial_tree.hop_to_next_node(None, initial_directions.directions.first().unwrap());
    let expected_second_point = Node { data: [C,C,C]};
    //next
    let calculated_next_point = initial_tree.hop_to_next_node(calculated_second_point, initial_directions.directions.get(1).unwrap());
    let expected_next_point = Node { data: [Z,Z,Z]};

    //then
    assert_eq!(calculated_second_point, Some(expected_second_point));
    assert_eq!(calculated_next_point, Some(expected_next_point))
}

#[test]
fn test_first_part_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = Tree::from_file(file_path);

    //when
    let mut calculated_hops = 0;
    let mut current_point: Option<Node> = None;
    while current_point != Some(Node { data: [Z,Z,Z]}) {
        initial_directions.directions.iter().for_each(|dir|
            match current_point {
                Some(Node { data: [Z,Z,Z]}) => {},
                _ => {
                    current_point = initial_tree.hop_to_next_node(current_point, &dir);
                    calculated_hops += 1;
                }
            }
        );
    };
    let expected_hops = 2;

    //then
    assert_eq!(calculated_hops, expected_hops);
}

#[test]
fn test_first_part_second_test_case() {
    //given
    let file_path = "secondary_test_case_num.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = Tree::from_file(file_path);

    //when
    let mut calculated_hops = 0;
    let mut current_point: Option<Node> = None;
    while current_point != Some(Node { data: [Z,Z,Z]}) {
        initial_directions.directions.iter().for_each(|dir|
            match current_point {
                Some(Node { data: [Z,Z,Z]}) => {},
                _ => {
                    current_point = initial_tree.hop_to_next_node(current_point, &dir);
                    calculated_hops += 1;
                }
            }
        );
    };
    let expected_hops = 6;

    //then
    assert_eq!(calculated_hops, expected_hops);
}

#[test]
fn test_first_part_input() {
    //given
    let file_path = "input.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = Tree::from_file(file_path);

    //when
    let mut calculated_hops = 0;
    let mut current_point: Option<Node> = None;
    while current_point != Some(Node { data: [Z,Z,Z]}) {
        initial_directions.directions.iter().for_each(|dir|
            match current_point {
                Some(Node { data: [Z,Z,Z]}) => {},
                _ => {
                    current_point = initial_tree.hop_to_next_node(current_point, &dir);
                    calculated_hops += 1;
                }
            }
        );
    };
    let expected_hops = 16579;

    //then
    assert_eq!(calculated_hops, expected_hops);
    println!(
        "{:=<75}\nThe first half of Day-8 puzzle \nSteps/hops needed to reach the destination point: {:?}",
        "", calculated_hops
    );
}

#[test]
fn  test_second_part_next_hop_multiple_nodes_third_test_case() {
    //given
    let file_path = "third_test_case.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = Tree::from_file(file_path);
    let dest_node = Node {data: [Z,Z,Z]};

    //when
    let mut calculated_hops = 0usize;
    let mut current_points: Vec<Node> = initial_tree.find_staring_points();
    println!(
        "{:=<75}\nThe second half of Day-8 puzzle \nWe have {} starting points in {}:\n{:?}",
        "", current_points.len(), file_path, current_points
    );
    while !current_points.iter().all(|p| p == &dest_node ) {
        initial_directions.directions.iter().for_each(|dir|
          {
              initial_tree.next_hop_multiple_nodes(&mut current_points, &dir);
              calculated_hops += 1;
          }
        );
    };
    let expected_hops = 6;

    //then
    println!(
        "we're reached the destination points: \n{:?}\nSteps/hops needed to reach the destination point: {:?}",
        current_points, calculated_hops
    );
    assert_eq!(calculated_hops, expected_hops);
}

#[test]
fn test_second_part_stepping_to_dest_node_third_test_case() {
    //given
    let file_path = "third_test_case.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = Tree::from_file(file_path);
    let dest_node = Node {data: [Z, Z, Z]};

    //when
    let mut current_points =
        initial_tree
            .find_staring_points()
            .iter()
            .map(|p| (0, *p) ).collect::<Vec<(usize, Node)>>();
    let expected_hops = 6usize;
    println!(
        "{:=<75}\nThe second half of Day-8 puzzle \nWe have {} starting points in {}:\n{:?}",
        "", current_points.clone().len(), file_path, current_points.clone()
    );
    let max_current_point = initial_tree.sync_stepping_to_dest_node(&mut current_points, &dest_node, &initial_directions);
    let calculated_hops = max_current_point.0.clone();

    //then
    println!(
        "we're reached the destination points: \n{:?}\nSteps/hops needed to reach the destination point: {:?}",
        current_points.clone(), calculated_hops
    );
    assert_eq!(calculated_hops, expected_hops);
}

#[test]
fn test_second_part_lcm_predict_input() {
    //given
    let file_path = "input.txt";
    let initial_directions = Instructions::from_file(file_path);
    let initial_tree = Tree::from_file(file_path);
    let dest_node = Node {data: [Z, Z, Z]};

    //when
    let mut current_points: Vec<(usize, Node)> = initial_tree.find_staring_points().iter().map(|p| (0, *p) ).collect::<Vec<(usize, Node)>>();
    let expected_hops = 12_927_600_769_609usize;
    println!(
        "{:=<75}\nThe second half of Day-8 puzzle \nWe have {} starting points in {}",
        "", current_points.clone().len(), file_path,
    );
    let calculated_hops = initial_tree.predict_steps_count_to_destinations(&mut current_points, &dest_node, &initial_directions);

    //then
    println!(
        "Predicted amount of steps/hops needed to reach the destination points simultaneously: {:?}",
        calculated_hops
    );
    assert_eq!(calculated_hops, expected_hops);
}


fn helper_get_initial_test_case_tree() -> Tree {
    Tree { 
        branches: BTreeSet::from(
            [
                Branch { value: Node { data: [A,A,A] }, left: Node { data: [B,B,B] }, right: Node { data: [C,C,C] } },
                Branch { value: Node { data: [B,B,B] }, left: Node { data: [D,D,D] }, right: Node { data: [E,E,E] } },
                Branch { value: Node { data: [C,C,C] }, left: Node { data: [Z,Z,Z] }, right: Node { data: [G,G,G] } },
                Branch { value: Node { data: [D,D,D] }, left: Node { data: [D,D,D] }, right: Node { data: [D,D,D] } },
                Branch { value: Node { data: [E,E,E] }, left: Node { data: [E,E,E] }, right: Node { data: [E,E,E] } },
                Branch { value: Node { data: [G,G,G] }, left: Node { data: [G,G,G] }, right: Node { data: [G,G,G] } },
                Branch { value: Node { data: [Z,Z,Z] }, left: Node { data: [Z,Z,Z] }, right: Node { data: [Z,Z,Z] } }
            ]
        )
    }
}

// #[test]
// fn test() {
//     //given
//
//     //when
//
//     //then
// }