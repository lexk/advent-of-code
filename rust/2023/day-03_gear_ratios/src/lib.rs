use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;
use crate::SchemeDotT::{NUM, SYM};

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub enum SchemeDotT {NUM, SYM}

impl SchemeDotT{
    pub fn from_char(data: &char) -> Option<SchemeDotT> {
        match data {
        &num if num.is_digit(10) => {Some(NUM)},
        &sym if sym != '.' && !sym.is_whitespace() => { Some(SYM) },
        _ => None}
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct Symbol {
    pub part_num: u8,
    pub y: usize,
    pub x: usize,
    pub t: SchemeDotT
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct Partition {
    pub serial: u32,
    pub raw_symbols: Vec<Symbol>
}

impl Partition {
    pub fn from_vec_sym(sym: &Vec<Symbol>) -> Partition {
        Partition{
            serial: sym.iter()
                .map(|num| num.part_num as char)
                .collect::<String>()
                .parse()
                .unwrap(),
            raw_symbols: sym.clone()
        }
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct Gear {
    pub connector: Symbol,
    pub parts: Vec<Partition>
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct GearData{
    pub serial: Vec<Gear>
}

impl GearData{
    pub fn from_part_data_raw_scheme(part_data: PartData, rw_sch: &RawScheme) -> GearData {
        GearData {
            serial:
            rw_sch.sch_lines.iter()
                .filter(|sym| sym.part_num == 42 )
                .filter_map(|con| GearData::validate_gear(&part_data, con).ok())
                .collect::<Vec<Gear>>()
        }
    }
    
    pub fn validate_gear(part_data: &PartData, con: &Symbol) -> Result<Gear, ()> {
        match part_data.serial.iter().fold(
            Vec::<Partition>::new(),
            | mut gear_candidate, cand_part| {
                if (
                        con.x.abs_diff(cand_part.raw_symbols.first().unwrap().x) <= 1 ||
                        con.x.abs_diff(cand_part.raw_symbols.last().unwrap().x) <= 1
                    ) &&
                    (con.y.abs_diff(cand_part.raw_symbols.first().unwrap().y) <= 1)
                 { gear_candidate.push(cand_part.clone()) };
                gear_candidate
            }
        ) {
            gear if gear.iter().len() >= 2 => Ok(Gear {connector: con.clone(), parts: gear}),
            _ => Err(())
        }
    }
}



#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct PartData {
    pub serial: Vec<Partition>
}

impl PartData {
    pub fn from_raw_scheme(rw_sch: &RawScheme) -> PartData {
        PartData {
            serial: rw_sch.sch_lines.iter()
                .fold(
                    (Vec::<Partition>::new(), Vec::<Symbol>::new()),
                    |mut res, symb| {
                        match symb.t {
                            NUM if symb.x.abs_diff(res.1.last().unwrap_or(symb).x) <= 1 => { res.1.push(symb.clone()) },
                            _ => {
                                if !res.1.is_empty() {
                                    match PartData::validate_partition(&rw_sch, &res.1) {
                                        None => {}
                                        Some(part) => {res.0.push(part)}
                                    };
                                    res.1.drain(..);
                                    if symb.t == NUM { res.1.push(symb.clone()) }
                                }
                            }
                        };
                        if symb == rw_sch.sch_lines.last().unwrap() {
                            match PartData::validate_partition(&rw_sch, &res.1) {
                                None => {}
                                Some(part) => {res.0.push(part)}
                            };
                            res.1.drain(..);
                        }
                        res
                    }
                ).0
        }
    }

    pub fn validate_partition(rw_sch: &RawScheme, part: &Vec<Symbol>) -> Option<Partition> {
        match rw_sch.sch_lines.iter()
            .find(
                |sym|
                    sym.t == SYM &&
                    (
                        (sym.x.abs_diff(part.first().unwrap().x) <= 1 || sym.x.abs_diff(part.last().unwrap().x) <= 1) &&
                        (sym.y.abs_diff(part.first().unwrap().y) <= 1)
                    )
            ) {
            None => None,
            Some(_) => Some(Partition::from_vec_sym(part))
        }
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct SchemeLine {
    pub data: Vec<Symbol>
}

impl SchemeLine {
    pub fn from_str(data: &str, y: usize) -> Result<SchemeLine, ()> {
        match data.chars()
            .enumerate()
            .fold(Vec::<Symbol>::new(), |mut line, sym| {
                match SchemeDotT::from_char(&sym.1) {
                    None => None,
                    Some(dot_t) => Some(line.push(Symbol{part_num: sym.1 as u8, y, x: sym.0, t: dot_t }))
                };
                line
            }) {
            sym_vec if sym_vec.len() > 0 => Ok(SchemeLine{data: sym_vec}),
            _ => Err(())
        }
    }
}

#[derive(Clone, Eq, PartialEq, PartialOrd, Debug)]
pub struct RawScheme {
    pub sch_lines: Vec<Symbol>
}

impl RawScheme {
    pub fn from_vec_scheme_lines(vec_sch_ln: Vec<SchemeLine>) -> RawScheme {
        RawScheme{
            sch_lines: vec_sch_ln.iter()
                .fold(Vec::<Symbol>::new(), |mut scheme, line| {
                    scheme.extend(line.data.clone());
                    scheme
                })
        }
    }
}

pub fn partition_engine_scheme(file_path: &str) -> Option<(PartData, RawScheme)> {
    match read_lines(file_path) {
        Ok(file_exists) =>
            Some({
                let rw_sch = RawScheme::from_vec_scheme_lines(
                    file_exists
                    .enumerate()
                    .filter_map(|line| SchemeLine::from_str(line.1.unwrap().as_ref(), line.0).ok())
                    .collect::<Vec<SchemeLine>>()
                );
                (PartData::from_raw_scheme(&rw_sch), rw_sch)
            }),
        Err(_) => { println!("File not found, double-check the name!"); None }
    }
}

pub fn gear_engine_scheme(file_path: &str) -> Option<GearData> {
    match partition_engine_scheme(file_path) {
        None => None,
        Some((part_data, rw_sch)) => { Some(GearData::from_part_data_raw_scheme(part_data, &rw_sch))
        }
    }
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}