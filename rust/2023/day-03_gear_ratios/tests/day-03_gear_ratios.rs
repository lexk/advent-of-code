use day_03_gear_ratios::SchemeDotT::{NUM, SYM};
use day_03_gear_ratios::{
    gear_engine_scheme, partition_engine_scheme, read_lines, Gear, GearData, PartData, Partition,
    RawScheme, SchemeDotT, SchemeLine, Symbol,
};

#[test]
fn test_scheme_dot_t_from_char() {
    //given
    let num = '9';
    let sym = '#';
    let letter = 'a';
    let dot = '.';

    //when
    let expected_num_dot_t = Some(NUM);
    let expected_sym_dot_t = Some(SYM);
    let expected_letter_dot_t = Some(SYM);
    let expected_dot_dot_t = None;

    let calculated_num_dot_t = SchemeDotT::from_char(&num);
    let calculated_sym_dot_t = SchemeDotT::from_char(&sym);
    let calculated_letter_dot_t = SchemeDotT::from_char(&letter);
    let calculated_dot_dot_t = SchemeDotT::from_char(&dot);

    //then
    assert_eq!(calculated_sym_dot_t, expected_sym_dot_t);
    assert_eq!(calculated_num_dot_t, expected_num_dot_t);
    assert_eq!(calculated_letter_dot_t, expected_letter_dot_t);
    assert_eq!(calculated_dot_dot_t, expected_dot_dot_t)
}

#[test]
fn test_scheme_line_from_str() {
    //given
    let num = "99";
    let sym = "!#";
    let letter = "ba";
    let dot = "..";

    let mixed_num = "9 9";
    let mixed_sym = "@+.!";
    let mixed_letter = "9 s 9";
    let mixed_dot = "..... ..";

    //when
    let expected_num = Ok(SchemeLine {
        data: vec![
            Symbol {
                part_num: 57,
                y: 0,
                x: 0,
                t: NUM,
            },
            Symbol {
                part_num: 57,
                y: 0,
                x: 1,
                t: NUM,
            },
        ],
    });
    let expected_sym = Ok(SchemeLine {
        data: vec![
            Symbol {
                part_num: 33,
                y: 0,
                x: 0,
                t: SYM,
            },
            Symbol {
                part_num: 35,
                y: 0,
                x: 1,
                t: SYM,
            },
        ],
    });
    let expected_letter = Ok(SchemeLine {
        data: vec![
            Symbol {
                part_num: 98,
                y: 0,
                x: 0,
                t: SYM,
            },
            Symbol {
                part_num: 97,
                y: 0,
                x: 1,
                t: SYM,
            },
        ],
    });
    let expected_dot: Result<SchemeLine, _> = Err(());
    let expected_mixed_num = Ok(SchemeLine {
        data: vec![
            Symbol {
                part_num: 57,
                y: 0,
                x: 0,
                t: NUM,
            },
            Symbol {
                part_num: 57,
                y: 0,
                x: 2,
                t: NUM,
            },
        ],
    });
    let expected_mixed_sym = Ok(SchemeLine {
        data: vec![
            Symbol {
                part_num: 64,
                y: 0,
                x: 0,
                t: SYM,
            },
            Symbol {
                part_num: 43,
                y: 0,
                x: 1,
                t: SYM,
            },
            Symbol {
                part_num: 33,
                y: 0,
                x: 3,
                t: SYM,
            },
        ],
    });
    let expected_mixed_letter = Ok(SchemeLine {
        data: vec![
            Symbol {
                part_num: 57,
                y: 0,
                x: 0,
                t: NUM,
            },
            Symbol {
                part_num: 115,
                y: 0,
                x: 2,
                t: SYM,
            },
            Symbol {
                part_num: 57,
                y: 0,
                x: 4,
                t: NUM,
            },
        ],
    });
    let expected_mixed_dot: Result<SchemeLine, _> = Err(());

    let calculated_num = SchemeLine::from_str(num, 0);
    let calculated_sym = SchemeLine::from_str(sym, 0);
    let calculated_letter = SchemeLine::from_str(letter, 0);
    let calculated_dot = SchemeLine::from_str(dot, 0);
    let calculated_mixed_num = SchemeLine::from_str(mixed_num, 0);
    let calculated_mixed_sym = SchemeLine::from_str(mixed_sym, 0);
    let calculated_mixed_letter = SchemeLine::from_str(mixed_letter, 0);
    let calculated_mixed_dot = SchemeLine::from_str(mixed_dot, 0);

    //then
    assert_eq!(calculated_num, expected_num);
    assert_eq!(calculated_sym, expected_sym);
    assert_eq!(calculated_letter, expected_letter);
    assert_eq!(calculated_dot, expected_dot);
    assert_eq!(calculated_mixed_num, expected_mixed_num);
    assert_eq!(calculated_mixed_sym, expected_mixed_sym);
    assert_eq!(calculated_mixed_letter, expected_mixed_letter);
    assert_eq!(calculated_mixed_dot, expected_mixed_dot);
}

#[test]
fn test_scheme_raw_from_vec_scheme_lines() {
    //given
    let file_path = "initial_test_case.txt";
    let first_line = SchemeLine::from_str(
        read_lines(file_path)
            .unwrap()
            .next()
            .unwrap()
            .unwrap()
            .as_ref(),
        0,
    );
    let last_line = SchemeLine::from_str(
        read_lines(file_path)
            .unwrap()
            .last()
            .unwrap()
            .unwrap()
            .as_ref(),
        1,
    );

    //when
    let calculate_raw_scheme_first_line =
        RawScheme::from_vec_scheme_lines(vec![first_line.clone().unwrap()]);
    let calculate_raw_scheme_two_lines =
        RawScheme::from_vec_scheme_lines(vec![first_line.clone().unwrap(), last_line.unwrap()]);
    let expected_raw_scheme_first_line = RawScheme {
        sch_lines: vec![
            Symbol {
                part_num: 52,
                y: 0,
                x: 0,
                t: NUM,
            },
            Symbol {
                part_num: 54,
                y: 0,
                x: 1,
                t: NUM,
            },
            Symbol {
                part_num: 55,
                y: 0,
                x: 2,
                t: NUM,
            },
            Symbol {
                part_num: 49,
                y: 0,
                x: 5,
                t: NUM,
            },
            Symbol {
                part_num: 49,
                y: 0,
                x: 6,
                t: NUM,
            },
            Symbol {
                part_num: 52,
                y: 0,
                x: 7,
                t: NUM,
            },
        ],
    };
    let expected_raw_scheme_two_lines = RawScheme {
        sch_lines: vec![
            Symbol {
                part_num: 52,
                y: 0,
                x: 0,
                t: NUM,
            },
            Symbol {
                part_num: 54,
                y: 0,
                x: 1,
                t: NUM,
            },
            Symbol {
                part_num: 55,
                y: 0,
                x: 2,
                t: NUM,
            },
            Symbol {
                part_num: 49,
                y: 0,
                x: 5,
                t: NUM,
            },
            Symbol {
                part_num: 49,
                y: 0,
                x: 6,
                t: NUM,
            },
            Symbol {
                part_num: 52,
                y: 0,
                x: 7,
                t: NUM,
            },
            Symbol {
                part_num: 54,
                y: 1,
                x: 1,
                t: NUM,
            },
            Symbol {
                part_num: 54,
                y: 1,
                x: 2,
                t: NUM,
            },
            Symbol {
                part_num: 52,
                y: 1,
                x: 3,
                t: NUM,
            },
            Symbol {
                part_num: 53,
                y: 1,
                x: 5,
                t: NUM,
            },
            Symbol {
                part_num: 57,
                y: 1,
                x: 6,
                t: NUM,
            },
            Symbol {
                part_num: 56,
                y: 1,
                x: 7,
                t: NUM,
            },
        ],
    };

    //then
    assert_eq!(
        calculate_raw_scheme_first_line,
        expected_raw_scheme_first_line
    );
    assert_eq!(
        calculate_raw_scheme_two_lines,
        expected_raw_scheme_two_lines
    );
}

#[test]
fn test_partition_from_vec_sym() {
    //given
    let file_path = "initial_test_case.txt";
    let first_line = SchemeLine::from_str(
        read_lines(file_path)
            .unwrap()
            .next()
            .unwrap()
            .unwrap()
            .as_ref(),
        0,
    );

    //when
    let calculated_partition = Partition::from_vec_sym(&first_line.unwrap().data);
    let expected_partition = Partition {
        serial: 467114,
        raw_symbols: vec![
            Symbol {
                part_num: 52,
                y: 0,
                x: 0,
                t: NUM,
            },
            Symbol {
                part_num: 54,
                y: 0,
                x: 1,
                t: NUM,
            },
            Symbol {
                part_num: 55,
                y: 0,
                x: 2,
                t: NUM,
            },
            Symbol {
                part_num: 49,
                y: 0,
                x: 5,
                t: NUM,
            },
            Symbol {
                part_num: 49,
                y: 0,
                x: 6,
                t: NUM,
            },
            Symbol {
                part_num: 52,
                y: 0,
                x: 7,
                t: NUM,
            },
        ],
    };

    //then
    assert_eq!(calculated_partition, expected_partition);
}

#[test]
fn test_partition_engine_scheme_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";
    //when
    let calculated_part_data = partition_engine_scheme(file_path).unwrap().0;
    let expected_part_data = PartData {
        serial: vec![
            Partition {
                serial: 467,
                raw_symbols: vec![
                    Symbol {
                        part_num: 52,
                        y: 0,
                        x: 0,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 54,
                        y: 0,
                        x: 1,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 55,
                        y: 0,
                        x: 2,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 35,
                raw_symbols: vec![
                    Symbol {
                        part_num: 51,
                        y: 2,
                        x: 2,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 53,
                        y: 2,
                        x: 3,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 633,
                raw_symbols: vec![
                    Symbol {
                        part_num: 54,
                        y: 2,
                        x: 6,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 51,
                        y: 2,
                        x: 7,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 51,
                        y: 2,
                        x: 8,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 617,
                raw_symbols: vec![
                    Symbol {
                        part_num: 54,
                        y: 4,
                        x: 0,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 49,
                        y: 4,
                        x: 1,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 55,
                        y: 4,
                        x: 2,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 592,
                raw_symbols: vec![
                    Symbol {
                        part_num: 53,
                        y: 6,
                        x: 2,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 57,
                        y: 6,
                        x: 3,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 50,
                        y: 6,
                        x: 4,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 755,
                raw_symbols: vec![
                    Symbol {
                        part_num: 55,
                        y: 7,
                        x: 6,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 53,
                        y: 7,
                        x: 7,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 53,
                        y: 7,
                        x: 8,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 664,
                raw_symbols: vec![
                    Symbol {
                        part_num: 54,
                        y: 9,
                        x: 1,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 54,
                        y: 9,
                        x: 2,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 52,
                        y: 9,
                        x: 3,
                        t: NUM,
                    },
                ],
            },
            Partition {
                serial: 598,
                raw_symbols: vec![
                    Symbol {
                        part_num: 53,
                        y: 9,
                        x: 5,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 57,
                        y: 9,
                        x: 6,
                        t: NUM,
                    },
                    Symbol {
                        part_num: 56,
                        y: 9,
                        x: 7,
                        t: NUM,
                    },
                ],
            },
        ],
    };

    let calculated_parts_serial_sum = calculated_part_data
        .serial
        .iter()
        .map(|part| part.serial)
        .sum::<u32>();
    let expected_parts_serial_sum = 4361;

    //then
    assert_eq!(calculated_part_data, expected_part_data);
    assert_eq!(calculated_parts_serial_sum, expected_parts_serial_sum);
}

#[test]
fn test_partition_engine_scheme_input() {
    //given
    let file_path = "input.txt";

    //when
    let calculated_part_data = partition_engine_scheme(file_path).unwrap().0;
    let calculated_parts_serial_sum = calculated_part_data
        .serial
        .iter()
        .map(|part| part.serial)
        .sum::<u32>();
    let expected_parts_serial_sum = 517_021;

    //then
    assert_eq!(calculated_parts_serial_sum, expected_parts_serial_sum);

    println!(
        "The first half of Day-3 puzzle \nSum of engines part serials: {:?}",
        calculated_parts_serial_sum
    );
}

#[test]
fn test_gear_engine_scheme_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";

    //when
    let calculated_gear_data = gear_engine_scheme(file_path).unwrap();
    let calculated_gear_first_ratio = calculated_gear_data
        .serial
        .first()
        .unwrap()
        .parts
        .iter()
        .map(|part| part.serial)
        .product::<u32>();
    let expected_gear_data = GearData {
        serial: vec![
            Gear {
                connector: Symbol {
                    part_num: 42,
                    y: 1,
                    x: 3,
                    t: SYM,
                },
                parts: vec![
                    Partition {
                        serial: 467,
                        raw_symbols: vec![
                            Symbol {
                                part_num: 52,
                                y: 0,
                                x: 0,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 54,
                                y: 0,
                                x: 1,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 55,
                                y: 0,
                                x: 2,
                                t: NUM,
                            },
                        ],
                    },
                    Partition {
                        serial: 35,
                        raw_symbols: vec![
                            Symbol {
                                part_num: 51,
                                y: 2,
                                x: 2,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 53,
                                y: 2,
                                x: 3,
                                t: NUM,
                            },
                        ],
                    },
                ],
            },
            Gear {
                connector: Symbol {
                    part_num: 42,
                    y: 8,
                    x: 5,
                    t: SYM,
                },
                parts: vec![
                    Partition {
                        serial: 755,
                        raw_symbols: vec![
                            Symbol {
                                part_num: 55,
                                y: 7,
                                x: 6,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 53,
                                y: 7,
                                x: 7,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 53,
                                y: 7,
                                x: 8,
                                t: NUM,
                            },
                        ],
                    },
                    Partition {
                        serial: 598,
                        raw_symbols: vec![
                            Symbol {
                                part_num: 53,
                                y: 9,
                                x: 5,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 57,
                                y: 9,
                                x: 6,
                                t: NUM,
                            },
                            Symbol {
                                part_num: 56,
                                y: 9,
                                x: 7,
                                t: NUM,
                            },
                        ],
                    },
                ],
            },
        ],
    };

    let expected_gear_first_ratio = 16_345;
    //then
    assert_eq!(calculated_gear_data, expected_gear_data);
    assert_eq!(calculated_gear_first_ratio, expected_gear_first_ratio)
}

#[test]
fn test_gear_engine_scheme_input() {
    //given
    let file_path = "input.txt";

    //when
    let calculated_gear_data = gear_engine_scheme(file_path).unwrap();
    let calculated_gear_ratio_sum = calculated_gear_data
        .serial
        .iter()
        .map(|gear| gear.parts.iter().map(|part| part.serial).product::<u32>())
        .sum::<u32>();
    let expected_gear_ratio_sum = 81_296_995;

    //then
    assert_eq!(calculated_gear_ratio_sum, expected_gear_ratio_sum);

    println!(
        "The second half of Day-3 puzzle \nSum of engines gears ratio product: {:?}",
        calculated_gear_ratio_sum
    );
}

// #[test]
// fn test() {
//     //given
//
//     //when
//
//     //then
// }
