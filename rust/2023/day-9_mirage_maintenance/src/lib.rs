use std::fs::File;
use std::io;
use std::io::BufRead;
use std::path::Path;

pub enum Direction {Right, Left}
pub fn seq_lines(line: &str) -> Vec<Vec<i32>> {
    fn deviation(seq_vec: &Vec<i32>) -> Vec<i32> {
        seq_vec.windows(2).map(|w| w.last().unwrap() - w.first().unwrap()).collect()
    }
    let mut result = vec![
        line
            .split_terminator(&[' '][..])
            .map(|num| num.parse::<i32>().unwrap()).collect::<Vec<i32>>()
    ];
    while result.last().unwrap().iter().any(|it| it != &0i32) {
        result.push(deviation(result.last().unwrap()))
    }
    result
}

pub fn predict_lst_seq(seq_dev_vec: &Vec<Vec<i32>>) -> i32 {
    seq_dev_vec.iter().fold(0i32, |sum, focus_vec| {
        sum + focus_vec.last().unwrap()
    })
}
pub fn predict_fst_seq(seq_dev_vec: &Vec<Vec<i32>>) -> i32 {
    seq_dev_vec.iter().rev().fold(0i32, |sum, focus_vec| {
        focus_vec.first().unwrap() - sum
    })
}

pub fn sequence_pred(file_path: &str, direction: Direction) -> Option<Vec<(usize, i32)>> {
    match read_lines(file_path) {
        Ok(file_exists) =>
            Some(
                file_exists
                    .map(|line|
                        match direction {
                            Direction::Left => {predict_fst_seq(&seq_lines(line.unwrap().as_ref()))}
                            Direction::Right => {predict_lst_seq(&seq_lines(line.unwrap().as_ref()))}
                        }
                    ).enumerate()
                    .collect::<Vec<(usize, i32)>>()
            ),
        Err(_) => {
            println!("File not found, double-check the name!");
            None
        }
    }
}

pub fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}