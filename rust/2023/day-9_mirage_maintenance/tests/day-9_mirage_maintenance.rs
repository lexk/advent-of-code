use day_9_mirage_maintenance::{predict_fst_seq, predict_lst_seq, read_lines, seq_lines, sequence_pred};
use day_9_mirage_maintenance::Direction::{Left, Right};

#[test]
fn test_seq_first_line() {
    //given
    let file_path = "initial_test_case.txt";

    //when
    let calculated_seq_deviations = seq_lines(read_lines(file_path).unwrap().next().unwrap().unwrap().as_ref());
    let expected_seq_deviations = 
        vec![
            vec![0, 3, 6, 9, 12, 15],
            vec![3, 3, 3, 3, 3],
            vec![0, 0, 0, 0]
        ];
    let calculated_prediction_lst = predict_lst_seq(&calculated_seq_deviations);
    let calculated_prediction_fst = predict_fst_seq(&calculated_seq_deviations);
    let expected_prediction_lst = 18;
    let expected_prediction_fst =  -3;


    //then
    assert_eq!(calculated_seq_deviations, expected_seq_deviations);
    assert_eq!(calculated_prediction_lst, expected_prediction_lst);
    assert_eq!(calculated_prediction_fst, expected_prediction_fst);
}

#[test]
fn test_sequence_pred_initial_test_case() {
    //given
    let file_path = "initial_test_case.txt";

    //when
    let calculated_sequence_pred_lst = sequence_pred(file_path, Right);
    let calculated_sequence_pred_fst = sequence_pred(file_path, Left);

    let expected_sequence_pred_lst = vec![(0, 18), (1, 28), (2, 68)];
    let expected_sequence_pred_fst = vec![(0, -3), (1, 0), (2, 5)];

    let calculated_sequence_pred_lst_sum = calculated_sequence_pred_lst.clone().unwrap().iter()
        .map(|pred| pred.1)
        .sum::<i32>();
    let calculated_sequence_pred_fst_sum = calculated_sequence_pred_fst.clone().unwrap().iter()
        .map(|pred| pred.1)
        .sum::<i32>();

    let expected_sequence_pred_lst_sum = 114i32;
    let expected_sequence_pred_fst_sum = 2i32;

    //then
    assert_eq!(calculated_sequence_pred_lst, Some(expected_sequence_pred_lst));
    assert_eq!(calculated_sequence_pred_lst_sum, expected_sequence_pred_lst_sum);

    assert_eq!(calculated_sequence_pred_fst, Some(expected_sequence_pred_fst));
    assert_eq!(calculated_sequence_pred_fst_sum, expected_sequence_pred_fst_sum);
}

#[test]
fn test_sequence_pred_secondary_test_case(){
    //given
    let file_path = "secondary_test_case.txt";

    //when
    let calculated_sequence_pred_fst = sequence_pred(file_path, Left);
    let expected_sequence_pred_fst = vec![(0, 5)];

    let calculated_seq_deviations = seq_lines(read_lines(file_path).unwrap().next().unwrap().unwrap().as_ref());
    let expected_seq_deviations =
        vec![
            vec![10,  13,  16,  21,  30,  45],
            vec![3,  3,  5,  9, 15],
            vec![0,  2,  4,  6],
            vec![2,  2,  2],
            vec![0,  0]
        ];

    let calculated_sequence_pred_fst_sum = calculated_sequence_pred_fst.clone().unwrap().iter()
        .map(|pred| pred.1)
        .sum::<i32>();
    let expected_sequence_pred_fst_sum = 5i32;

    //then
    assert_eq!(calculated_seq_deviations, expected_seq_deviations);
    assert_eq!(calculated_sequence_pred_fst, Some(expected_sequence_pred_fst));
    assert_eq!(calculated_sequence_pred_fst_sum, expected_sequence_pred_fst_sum);
}

#[test]
fn test_sequence_pred_input() {
    //given
    let file_path = "input.txt";

    //when
    let calculated_sequence_pred_lst = sequence_pred(file_path, Right);
    let calculated_sequence_pred_fst = sequence_pred(file_path, Left);

    let calculated_sequence_pred_lst_sum = calculated_sequence_pred_lst.clone().unwrap().iter()
        .map(|pred| pred.1)
        .sum::<i32>();
    let calculated_sequence_pred_fst_sum = calculated_sequence_pred_fst.clone().unwrap().iter()
        .map(|pred| pred.1)
        .sum::<i32>();
    let expected_sequence_pred_lst_sum = 1_708_206_096;
    let expected_sequence_pred_fst_sum = 1_050;

    //then
    assert_eq!(calculated_sequence_pred_lst_sum, expected_sequence_pred_lst_sum);
    println!(
        "The first half of Day-7 puzzle \nSum of predictions for next symbol: {:?}",
        calculated_sequence_pred_lst_sum
    );
    assert_eq!(calculated_sequence_pred_fst_sum, expected_sequence_pred_fst_sum);
    println!(
        "The second half of Day-7 puzzle \nSum of predictions for previous symbol: {:?}",
        calculated_sequence_pred_fst_sum
    );

}

// #[test]
// fn test() {
//     //given
//
//     //when
//
//     //then
// }